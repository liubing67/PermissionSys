package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */
@Data
public class TimingJob implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//ID
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;
	//任务类名
	private String className;
	//cron 表达式
	private String cronExpression;
	//状态：1暂停、0启用
	private String state;
	//描述
	private String params;
	//备注
	private String remark;
	//创建日期
	private Date createTime;
	//上次执行时间
	private Date prevExecuteTime;
	//下次执行时间
	private Date nextExecuteTime;

}
