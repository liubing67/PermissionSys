package com.abing.core.utils.rsatest;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * RSA加解密工具类
 *
 * @author jensvn@qq.com on 2017/11/3.
 * @version 1.0
 * @date 2017/11/3.
 */
public class RSAUtilTest {

    /**
     * 测试生成RSA密钥对
     */

    public void testGenKeyPair() throws Exception {
        RSAUtil.genKeyPair("test");
    }

    /**
     * 测试加解密
     */

    public static void main(String[] args) throws Exception {

        String prefix="guerlain";
        RSAUtil.genKeyPair("D:/"+prefix);

        String data = "11111111111111111111111111111";
        // 加载私钥、公钥
        PrivateKey privateKey = RSAUtil.loadPrivateKey("D:/"+prefix+"_privateKey.txt");
        PublicKey publicKey = RSAUtil.loadPublicKey("D:/"+prefix+"_publicKey.txt");
        // 数据加密
        byte[] encryptData = RSAUtil.encrypt(privateKey, data.getBytes());
        // 数据传输使用Base64编码传输字符串
        String encryptDataString = new BASE64Encoder().encode(encryptData);
        // 数据解密
        byte[] decryptData = RSAUtil.decrypt(publicKey, new BASE64Decoder().decodeBuffer(encryptDataString));
        System.out.println("privateKey:" + privateKey);
        System.out.println("publicKey:" + publicKey);
        System.out.println("加密前:" + data);
        System.out.println("加密后:" + encryptDataString);
        System.out.println("解密后:" + new String(decryptData));
    }

}
