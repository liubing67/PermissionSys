package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 角色
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysRole implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	@TableId(value = "role_id",type = IdType.AUTO)
	private Long roleId;
	//角色名称
	private String roleName;
	//角色标识
	private String roleSign;
	//备注
	private String remark;
	//创建用户id
	private Long userIdCreate;
	//创建时间
	private Date gmtCreate;
	//创建时间
	private Date gmtModified;

}
