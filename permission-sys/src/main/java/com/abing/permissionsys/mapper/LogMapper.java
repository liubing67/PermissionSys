package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-12 19:42:50
 */
@Mapper
public interface LogMapper extends BaseMapper<SysLog> {

}
