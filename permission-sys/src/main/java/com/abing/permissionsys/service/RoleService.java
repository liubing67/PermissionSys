package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysRoleVO;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface RoleService {


	PageResult list(SysRoleVO sysRoleVO, PageVal pageVal);

	int save(SysRoleVO role);

	int update(SysRoleVO role);

	int remove(Long id);

	List<Long> selectRoleByUserId(Long userId);

}
