package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.TimingJobVO;

/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */
public interface TimingJobService {

	/**
	 * 获取定时任务列表
	 * @param timingJobVO
	 * @param pageVal
	 * @return
	 */
	PageResult list(TimingJobVO timingJobVO, PageVal pageVal);
	/**
	 * 更新下次执行时间
	 * @param timingJob
	 * @return
	 */
	int updateTime(TimingJobVO timingJob);
	/**
	 * 保存定时任务
	 */
	int save(TimingJobVO timingJob);

	/**
	 * 更新定时任务
	 * @param timingJob
	 * @return
	 */
	int update(TimingJobVO timingJob);

	/**
	 * 删除定时任务
	 * @param id
	 * @return
	 */
	int remove(Long id);

	/**
	 * 立即执行
	 */
	void run(Long id);

	/**
	 * 暂停运行
	 */
	void pause(Long id);

	/**
	 * 恢复运行
	 */
	void resume(Long id);
}
