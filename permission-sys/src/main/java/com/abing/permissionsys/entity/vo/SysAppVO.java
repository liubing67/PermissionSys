package com.abing.permissionsys.entity.vo;

import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;


/**
 * 应用系统表
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:26
 */
@Data
public class SysAppVO extends BaseVO {
	private static final long serialVersionUID = 1L;
	
	//应用id
	private Long id;
	//应用的名称
	private String name;
	//项目名称
	private String appCode;
	//应用url
	private String url;
	//应用描述
	private String dese;

}
