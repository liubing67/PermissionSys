package com.abing.permissionsys.controller;

import com.abing.base.BaseResponse;
import com.abing.permissionsys.service.RoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RequestMapping("/roleMenu")
@RestController
@Slf4j
public class RoleMenuController extends BaseController {
	@Autowired
	RoleMenuService roleMenuService;


	/**
	 * 根据角色Id获取菜单
	 * @param roleId
	 * @return
	 */
	@PostMapping("/getMenuIdsByRoleId/{roleId}")
	@ResponseBody
	BaseResponse<List<Long>> getMenuIdsByRoleId(@PathVariable Long roleId) {
		List<Long> menuIdsList = roleMenuService.getMenuIdsByRoleId(roleId);
		log.info(menuIdsList.toString());
		return setResultSuccess(menuIdsList);
	}


	/**
	 * 保存菜单Id根据roleId
	 * @param map
	 * @return
	 */
	@PostMapping("/saveMenuByRoleId")
	@ResponseBody
	BaseResponse<List<Long>> saveMenuByRoleId(@RequestBody Map map) {
		try {
			String roleId=(String) map.get("roleId");
			List<String> menuIds= (List<String>) map.get("menuIds");
			roleMenuService.saveMenuByRoleId(roleId,menuIds);
			return setResultSuccess();
		}catch (Exception e){
			e.printStackTrace();
			return setResultError();
		}
	}
}
