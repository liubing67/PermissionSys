package com.abing.permissionsys.service;


import com.abing.permissionsys.entity.domain.Tree;
import com.abing.permissionsys.entity.vo.SysDeptVO;
import com.abing.permissionsys.entity.vo.dept.DeptVO;

import java.util.List;
import java.util.Map;

/**
 * 部门管理
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-27 14:28:36
 */
public interface DeptService {



	int count(Map<String, Object> map);

	int save(SysDeptVO sysDept);

	int update(SysDeptVO sysDept);

	int remove(Long deptId);

	List<DeptVO> getTree(SysDeptVO sysDeptVO);

	boolean checkDeptHasUser(Long deptId);

}
