package com.abing.permissionsys.controller;

import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.vo.SysMenuVO;
import com.abing.permissionsys.entity.vo.menu.MenuVo;
import com.abing.permissionsys.service.MenuService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author bootdo 1992lcg@163.com
 */
@RequestMapping("/menu")
@RestController
@Slf4j
public class MenuController extends BaseController {
	@Autowired
    MenuService menuService;

	@PostMapping("/save")
	@ResponseBody
	BaseResponse save(@RequestBody SysMenuVO menu) {
		if (menuService.save(menu) > 0) {
			return setResultSuccess();
		} else {
			return setResultError();
		}
	}

	@PostMapping("/update")
	@ResponseBody
	BaseResponse update(@RequestBody SysMenuVO sysMenuVO) {
		if (menuService.update(sysMenuVO) > 0) {
			return setResultSuccess();
		} else {
			return setResultError();
		}
	}

	@PostMapping("/remove/{id}")
	@ResponseBody
	BaseResponse remove(@PathVariable Long id) {
		if (menuService.remove(id) > 0) {
			return setResultSuccess();
		} else {
			return setResultError();
		}
	}


	/**
	 * 根据应用获取菜单
	 * @param sysMenuVO
	 * @return
	 */
	@RequestMapping("/getMenuByAppId")
	@ResponseBody
	BaseResponse<List<SysMenuVO>> getMenuByAppId(SysMenuVO sysMenuVO) {
		List<SysMenuVO> menuVoList = menuService.getMenuByAppId(sysMenuVO.getAppId());
		log.info(menuVoList.toString());
		return setResultSuccess(menuVoList);
	}

	/**
	 * 根据用户  应用获取菜单
	 * @param map
	 * @return
	 */
    @RequestMapping("/getUserMenuByAppId")
    @ResponseBody
	BaseResponse<List<MenuVo>> getUserMenuByAppId(@RequestBody Map map) {
		List<MenuVo> menuVoList = menuService.getUserMenuByAppId(getUserId(), Long.valueOf((String) map.get("appId")));
		log.info("menuVoList:{}", JSON.toJSON(menuVoList));
        return setResultSuccess(menuVoList);
    }

//	@RequestMapping("/getUserMenuByAppId")
//	@ResponseBody
//	BaseResponse<List<MenuVo>> getUserMenuByAppId(@RequestBody Map map) {
//
//		List<MenuVo> menuVoList=new ArrayList<>();
//		for  ( int  i  =   0 ; i  <   1 ; i ++ )
//		{
//			MenuVo menuVo=new MenuVo();
//			menuVo.setName("系统管理");
//			menuVo.setPath("/Layout");
//			menuVo.setRedirect("noredirect");
//			menuVo.setComponent("Layout");
//			menuVo.setAlwaysShow(true);
//			menuVo.setMeta(new MenuMetaVo("系统管理1","system"));
//			MenuVo menuVo1=new MenuVo();
//			menuVo1.setName("用户管理");
//			menuVo1.setPath("system/user/UserList");
//			menuVo1.setComponent("system/user/UserList");
//			menuVo1.setChildren(new ArrayList<>());
//			menuVo1.setMeta(new MenuMetaVo("用户管理1","github"));
//			List<MenuVo> menuVoList1=new ArrayList<>();
//			menuVoList1.add(menuVo1);
//			menuVo.setChildren(menuVoList1);
//			menuVoList.add(menuVo);
//		}
//		log.info("menuVoList:{}", JSON.toJSON(menuVoList));
//		return setResultSuccess(menuVoList);
//	}
}
