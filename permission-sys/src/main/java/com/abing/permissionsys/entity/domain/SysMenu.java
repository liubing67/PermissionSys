package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 菜单管理
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysMenu implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;
	//父菜单ID，一级菜单为0
	private Long parentId;
	//系统应用id
	private Long appId;
	//菜单名称
	private String name;
	//菜单URL
	private String url;
	//授权(多个用逗号分隔，如：user:list,user:create)
	private String perms;
	//类型   0：目录   1：菜单   2：按钮
	private Integer type;
	//菜单图标
	private String icon;
	//排序
	private Integer orderNum;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;

}
