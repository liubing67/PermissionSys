package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.TimingJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */
@Mapper
public interface TimingJobMapper extends BaseMapper<TimingJob> {


}
