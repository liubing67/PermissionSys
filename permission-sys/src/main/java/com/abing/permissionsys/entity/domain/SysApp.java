package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 应用系统表
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:26
 */
@Data
public class SysApp implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//应用id
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;
	//应用的名称
	private String name;
	//项目名称
	private String appCode;
	//应用url
	private String url;
	//应用描述
	private String dese;

}
