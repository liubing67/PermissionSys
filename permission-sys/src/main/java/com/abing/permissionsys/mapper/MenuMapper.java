package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 菜单管理
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 09:45:09
 */
@Mapper
public interface MenuMapper extends BaseMapper<SysMenu> {

	List<SysMenu> listMenuByUserId(@Param("userId") Long userId, @Param("appId")long appId);
	List<SysMenu> listMenuByAppId(@Param("appId")long appId);

	List<String> listUserPerms(Long id);
}
