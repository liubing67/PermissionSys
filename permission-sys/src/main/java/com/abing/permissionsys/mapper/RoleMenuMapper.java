package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 角色与菜单对应关系
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 11:08:59
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<SysRoleMenu> {


	List<Long> getMenuIdsByRoleId(Long roleId);

	int removeMenusByRoleId(Long roleId);

	int removeByMenuId(Long menuId);

}
