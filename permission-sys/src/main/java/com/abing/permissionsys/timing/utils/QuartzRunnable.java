package com.abing.permissionsys.timing.utils;

import com.abing.core.utils.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;

/**
 * 执行定时任务
 * @author /
 */
@Slf4j
public class QuartzRunnable implements Callable {

	private Object target;
	private Method method;

	QuartzRunnable(String beanName)
			throws NoSuchMethodException, SecurityException {
		this.target = SpringContextHolder.getBean(beanName);
		this.method = target.getClass().getDeclaredMethod("run");
	}

	@Override
	public Object call() throws Exception {
		ReflectionUtils.makeAccessible(method);
		method.invoke(target);
		return null;
	}
}
