package com.abing.permissionsys.controller;

import com.abing.base.BaseApiService;
import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysLogVO;
import com.abing.permissionsys.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 系统日志
 *
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-12 19:42:50
 */

@Controller
@RequestMapping("/log")
@Slf4j
public class LogController extends BaseApiService {
	@Autowired
	private LogService logService;


	@ResponseBody
	@RequestMapping(value = "/listPageLogInfo")
	public BaseResponse listPageLogInfo(SysLogVO sysLogVO, PageVal pageVal){
		//查询列表数据
		sysLogVO.setLogType("0");
		PageResult appDOIPage=logService.list(sysLogVO,pageVal);
		log.info("listPageLogInfo查询：{}",appDOIPage);
		return setResultSuccess(appDOIPage);
	}
	@ResponseBody
	@RequestMapping(value = "/listPageLogError")
	public BaseResponse listPageLogError(SysLogVO sysLogVO, PageVal pageVal){
		//查询列表数据
		sysLogVO.setLogType("1");
		PageResult appDOIPage=logService.list(sysLogVO,pageVal);
		log.info("listPageLogError查询：{}",appDOIPage);
		return setResultSuccess(appDOIPage);
	}
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	public BaseResponse save( SysLogVO sysLogVO){
		if(logService.save(sysLogVO)>0){
			return setResultSuccess();
		}
		return setResultError();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	public BaseResponse update( SysLogVO sysLogVO){
		logService.update(sysLogVO);
		return setResultSuccess();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	public BaseResponse remove( Long id){
		if(logService.remove(id)>0){
		return setResultSuccess();
		}
		return setResultError();
	}

}
