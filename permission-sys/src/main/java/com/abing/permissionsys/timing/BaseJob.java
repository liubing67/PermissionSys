package com.abing.permissionsys.timing;

import org.quartz.JobExecutionException;

public interface BaseJob  {
	public void run() ;
}

