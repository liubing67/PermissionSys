/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.abing.permissionsys.timing.utils;

import com.abing.permissionsys.entity.vo.TimingJobVO;
import com.abing.permissionsys.enums.TimingJobEnums;
import com.abing.permissionsys.exception.BadRequestException;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 定时任务工具类
 *
 * @author Mark sunlightcs@gmail.com
 */
@Component
public class ScheduleUtils {
    private final static String JOB_NAME = "TASK_";
    @Autowired
    private Scheduler scheduler;
    /**
     * 获取触发器key
     */
    public static TriggerKey getTriggerKey(Long jobId) {
        return TriggerKey.triggerKey(JOB_NAME + jobId);
    }
    
    /**
     * 获取jobKey
     */
    public static JobKey getJobKey(Long jobId) {
        return JobKey.jobKey(JOB_NAME + jobId);
    }

    /**
     * 获取表达式触发器
     */
    public CronTrigger getCronTrigger( Long jobId) {
        try {
            return (CronTrigger) scheduler.getTrigger(getTriggerKey(jobId));
        } catch (SchedulerException e) {
            throw new BadRequestException("获取定时任务CronTrigger出现异常", e);
        }
    }

    /**
     * 创建定时任务
     */
    public void createScheduleJob( TimingJobVO timingJob) {
        try {
        	//构建job信息
            JobDetail jobDetail = JobBuilder.newJob(ScheduleJob.class).withIdentity(getJobKey(timingJob.getId())).build();

            //表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(timingJob.getCronExpression()).withMisfireHandlingInstructionDoNothing();

            //按新的cronExpression表达式构建一个新的trigger
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(getTriggerKey(timingJob.getId())).withSchedule(scheduleBuilder).build();

            //放入参数，运行时的方法可以获取
            jobDetail.getJobDataMap().put(TimingJobVO.JOB_PARAM_KEY, timingJob);

            scheduler.scheduleJob(jobDetail, trigger);
            
            //暂停任务
            if(timingJob.getState().equals(TimingJobEnums.StateEnum.PAUSE.getIndex())){
            	pauseJob(timingJob.getId());
            }
        } catch (SchedulerException e) {
            throw new BadRequestException("创建定时任务失败", e);
        }
    }
    
    /**
     * 更新定时任务
     */
    public void updateScheduleJob(TimingJobVO timingJobVO) {
        try {
            TriggerKey triggerKey = getTriggerKey(timingJobVO.getId());

            //表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(timingJobVO.getCronExpression()).withMisfireHandlingInstructionDoNothing();

            CronTrigger trigger = getCronTrigger(timingJobVO.getId());
            
            //按新的cronExpression表达式重新构建trigger
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
            
            //参数
            trigger.getJobDataMap().put(TimingJobVO.JOB_PARAM_KEY, timingJobVO);
            
            scheduler.rescheduleJob(triggerKey, trigger);
            
            //暂停任务
            if(timingJobVO.getState().equals(TimingJobEnums.StateEnum.PAUSE.getIndex())){
            	pauseJob(timingJobVO.getId());
            }
            
        } catch (SchedulerException e) {
            throw new BadRequestException("更新定时任务失败", e);
        }
    }

    /**
     * 立即执行任务
     */
    public void run(TimingJobVO timingJobVO) {
        try {
        	//参数
        	JobDataMap dataMap = new JobDataMap();
        	dataMap.put(TimingJobVO.JOB_PARAM_KEY, timingJobVO);
        	
            scheduler.triggerJob(getJobKey(timingJobVO.getId()), dataMap);
        } catch (SchedulerException e) {
            throw new BadRequestException("立即执行定时任务失败", e);
        }
    }

    /**
     * 暂停任务
     */
    public void pauseJob(Long jobId) {
        try {
            scheduler.pauseJob(getJobKey(jobId));
        } catch (SchedulerException e) {
            throw new BadRequestException("暂停定时任务失败", e);
        }
    }

    /**
     * 恢复任务
     */
    public void resumeJob( Long jobId) {
        try {
            scheduler.resumeJob(getJobKey(jobId));
        } catch (SchedulerException e) {
            throw new BadRequestException("暂停定时任务失败", e);
        }
    }

    /**
     * 删除定时任务
     */
    public void deleteScheduleJob( Long jobId) {
        try {
            scheduler.deleteJob(getJobKey(jobId));
        } catch (SchedulerException e) {
            throw new BadRequestException("删除定时任务失败", e);
        }
    }
}
