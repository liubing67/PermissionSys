package com.abing.permissionsys.service.impl;

import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.PageNavigation;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.domain.SysRole;
import com.abing.permissionsys.entity.vo.SysRoleVO;
import com.abing.permissionsys.mapper.RoleMapper;
import com.abing.permissionsys.mapper.UserMapper;
import com.abing.permissionsys.mapper.UserRoleMapper;
import com.abing.permissionsys.service.RoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoleServiceImpl implements RoleService {


    @Autowired
    RoleMapper roleMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserRoleMapper userRoleMapper;


    @Override
    public List<Long> selectRoleByUserId(Long userId) {
        List<Long> rolesIds = userRoleMapper.selectRoleByUserId(userId);
        return rolesIds;
    }

    @Override
    public PageResult list(SysRoleVO sysRoleVO, PageVal pageVal) {
        QueryWrapper<SysRole> queryWrapper=new QueryWrapper<SysRole>();
        queryWrapper.eq(!StringUtils.isNullOrEmpty(sysRoleVO.getRoleName()),"roleName",sysRoleVO.getRoleName());
        IPage<SysRole> appDoList=roleMapper.selectPage(new PageNavigation<>(pageVal),queryWrapper);
        List<SysRoleVO> roleVOS= BeanUtil.doToVoList(appDoList.getRecords(),SysRoleVO.class);
        return new PageResult(appDoList,roleVOS);
    }

    @Override
    public int save(SysRoleVO role) {
        int count = roleMapper.insert(BeanUtil.voToDo(role,SysRole.class));
        return count;
    }

    @Override
    public int remove(Long id) {
        int count = roleMapper.deleteById(id);
        return count;
    }

    @Override
    public int update(SysRoleVO role) {
        int r = roleMapper.updateById(BeanUtil.voToDo(role,SysRole.class));
        return r;
    }
}
