package com.abing.permissionsys.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */
@Data
public class TimingJobLogVO implements Serializable {
	private static final long serialVersionUID = 1L;

	//ID
	private Long id;
	//任务类名
	private String className;
	//cron 表达式
	private String cronExpression;
	//状态：1暂停、0启用
	private String state;
	//描述
	private String params;
	//备注
	private String remark;
	//创建日期
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTimeStart;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTimeEnd;
	//上次执行时间
	private Date prevExecuteTime;
	//下次执行时间
	private Date nextExecuteTime;

	//耗时
	private String times;
	//错误信息
	private String errors;
}
