package com.abing.constants;

import lombok.Data;


public enum  HttpResCode {

    /**
     * Http请求返回码
     *  http请求码对应的信息
     */
    HttpResCode_200(200,"响应请求成功"),
    HttpResCode_500(500,"系统错误"),
    HttpResCode_201(201,"未关联QQ账号"),
    HttpResCode_401(401,"登录失效，请重新登录"),
    HttpResCode_203(203,"用户信息不存在");


    /**
     * Http请求返回码
     */
    private Integer code;
    /**
     * http请求码对应的信息
     */
    private String value;


    private HttpResCode(Integer code,String value){
        this.code=code;
        this.value=value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    /**
     * 根据value获取code
     * @param value
     * @return
     */
    public static Integer getCode(String value) {
        for (HttpResCode code : HttpResCode.values()) {
            if (code.getValue().equals(value)) {
                return code.code;
            }
        }
        return null;
    }

    /**
     * 根据code获取value
     * @param code
     * @return
     */
    public static String getValue(Integer code) {
        for (HttpResCode httpResCode : HttpResCode.values()) {
            if (httpResCode.getCode().equals(code)) {
                return httpResCode.value;
            }
        }
        return null;
    }
}
