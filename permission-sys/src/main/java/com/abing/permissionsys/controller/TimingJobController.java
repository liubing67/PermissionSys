package com.abing.permissionsys.controller;

import com.abing.base.BaseApiService;
import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.TimingJobVO;
import com.abing.permissionsys.service.TimingJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */

@RestController
@Slf4j
@RequestMapping("/timingJob")
public class TimingJobController extends BaseApiService {
	@Autowired
	private TimingJobService timingJobService;
	
	@ResponseBody
	@RequestMapping(value = "/listPage")
	public BaseResponse listPage(TimingJobVO timingJobVO, PageVal pageVal){
		//查询列表数据
		PageResult sysRole=timingJobService.list(timingJobVO,pageVal);
		log.info("timingJobVO查询：{}",sysRole);
		return setResultSuccess(sysRole);
	}
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	public BaseResponse save(@RequestBody TimingJobVO timingJob){
		if(timingJobService.save(timingJob)>0){
			return setResultSuccess();
		}
		return setResultError();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	public BaseResponse update(@RequestBody TimingJobVO timingJob){
		timingJobService.update(timingJob);
		return setResultSuccess();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove/{id}")
	@ResponseBody
	public BaseResponse remove(@PathVariable Long id){
		if(timingJobService.remove(id)>0){
		return setResultSuccess();
		}
		return setResultError();
	}
	/**
	 * 执行
	 */
	@PostMapping( "/execution/{id}")
	@ResponseBody
	public BaseResponse execution(@PathVariable Long id){
		timingJobService.run(id);
		return setResultSuccess();
	}
	/**
	 * 暂停定时任务
	 */
	@RequestMapping("/pause/{id}")
	public BaseResponse pause(@PathVariable Long id){
		timingJobService.pause(id);

		return setResultSuccess();
	}

	/**
	 * 恢复定时任务
	 */
	@RequestMapping("/resume/{id}")
	public BaseResponse resume(@PathVariable Long id){
		timingJobService.resume(id);
		return setResultSuccess();
	}
}
