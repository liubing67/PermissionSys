package com.abing.permissionsys.entity.vo;

import lombok.Data;

import java.io.Serializable;


/**
 * 用户与角色对应关系
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysUserRoleVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//用户ID
	private Long userId;
	//角色ID
	private Long roleId;

}
