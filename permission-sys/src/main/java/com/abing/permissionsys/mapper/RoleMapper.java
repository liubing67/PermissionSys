package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;

/**
 * 角色
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-02 20:24:47
 */
public interface RoleMapper extends BaseMapper<SysRole> {

}
