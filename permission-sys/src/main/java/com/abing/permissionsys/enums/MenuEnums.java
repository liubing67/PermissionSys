package com.abing.permissionsys.enums;

public class MenuEnums {


    public enum MenuTypeEnum{

        MenuTypeEnum_0("0","目录"),
        MenuTypeEnum_1("1","菜单"),
        MenuTypeEnum_2("2","按钮");
        private String index;
        private String name;


        MenuTypeEnum(String index,String name){
            this.index=index;
            this.name=name;
        }
        public static String getName(String index) {
            for (MenuTypeEnum code : MenuTypeEnum.values()) {
                if (code.getIndex().equals(index)) {
                    return code.name;
                }
            }
            return null;
        }

        public static String getIndex(String name) {
            for (MenuTypeEnum code : MenuTypeEnum.values()) {
                if (code.getName().equals(name)) {
                    return code.index;
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }
    }
}
