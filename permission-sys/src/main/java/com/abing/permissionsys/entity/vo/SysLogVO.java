package com.abing.permissionsys.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 系统日志
 *
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysLogVO implements Serializable {
	private static final long serialVersionUID = 1L;

	//
	private Long id;
	//用户id
	private Long userId;
	//用户名
	private String username;
	//用户操作
	private String operation;
	//响应时间
	private Long time;
	//请求方法
	private String method;
	//请求参数
	private String params;
	//IP地址
	private String ip;
	//创建时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date gmtCreate;
	//错误详细
	private String exceptionDetail;
	//日志类型（错误日志，操作日志）
	private String logType;
	//地址来源
	private String address;
	//浏览器类型
	private String browser;
	//创建开始
	private Date startTime;
	//创建结束
	private Date endTime;
	public SysLogVO() {

	}
	public SysLogVO(String logType, Long time) {
		this.logType = logType;
		this.time = time;
	}

}
