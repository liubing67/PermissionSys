package com.abing.permissionsys.mapper;

import com.abing.permissionsys.cache.MybatisRedisCache;
import com.abing.permissionsys.entity.domain.SysApp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 应用系统表
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-06-21 10:54:03
 */
@CacheNamespace(implementation = MybatisRedisCache.class)
public interface AppMapper extends BaseMapper<SysApp> {


}
