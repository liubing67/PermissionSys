package com.abing.core.utils;

import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

/**重试工具类
 * @author : liubing
 * @date : 2021/5/14 19:17
 */
public class RetryUtils {

    public static void main(String[] args) throws Exception {
        new RetryUtils().sendMsg("", null, null);
    }

    public void sendMsg(String msg, String phone, String[] arr) throws Exception {

        RetryTemplate template = new RetryTemplate();
        // 策略
        SimpleRetryPolicy policy = new SimpleRetryPolicy();
        //最大重试次数
        policy.setMaxAttempts(3);
        template.setRetryPolicy(policy);

        ExponentialBackOffPolicy exponen = new ExponentialBackOffPolicy();
        //初始时间间隔
        exponen.setInitialInterval(3000);
        //每次重试间隔倍数
        exponen.setMultiplier(2);
        template.setBackOffPolicy(exponen);
        String result = template.execute(arg0 -> {
            try {
                if (1 == 1) {
                    System.out.println(DateUtils.formatDateTime(new Date()) + "---123");
                    Thread.sleep(3000);
                    throw new Exception();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                throw new Exception();
            }
            return "111";
        }, context -> "recovery callback");
        System.out.println("----" + result);
    }
}
