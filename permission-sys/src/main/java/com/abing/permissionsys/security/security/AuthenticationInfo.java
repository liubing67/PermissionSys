package com.abing.permissionsys.security.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author Zheng Jie
 * @date 2018-11-23
 * 返回token
 */
@Data
@AllArgsConstructor
public class AuthenticationInfo implements Serializable {

    private final String token;

    private final JwtUser user;
}
