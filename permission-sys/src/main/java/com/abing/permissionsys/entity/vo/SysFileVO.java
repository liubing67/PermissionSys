package com.abing.permissionsys.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 文件上传
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:26
 */
@Data
public class SysFileVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//文件类型
	private Integer type;
	//URL地址
	private String url;
	//创建时间
	private Date createDate;

}
