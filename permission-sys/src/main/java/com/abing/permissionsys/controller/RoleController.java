package com.abing.permissionsys.controller;

import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysRoleVO;
import com.abing.permissionsys.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RequestMapping("/role")
@RestController
@Slf4j
public class RoleController extends BaseController {
	@Autowired
    RoleService roleService;


	@ResponseBody
	@RequestMapping(value = "/listPage")
	public BaseResponse listPage(SysRoleVO sysRoleVO, PageVal pageVal){
		//查询列表数据
		PageResult sysRole=roleService.list(sysRoleVO,pageVal);
		log.info("SysRoleVO查询：{}",sysRole);
		return setResultSuccess(sysRole);
	}


	@RequestMapping("/save")
	@ResponseBody()
	BaseResponse save(@RequestBody SysRoleVO role) {

        role.setUserIdCreate(getUserId());
        role.setGmtCreate(new Date());
		if (roleService.save(role) > 0) {
			return setResultSuccess();
		} else {
			return setResultError("保存失败");
		}
	}

	@RequestMapping("/update")
	@ResponseBody()
	BaseResponse update(@RequestBody SysRoleVO role) {
		role.setGmtModified(new Date());
		if (roleService.update(role) > 0) {
			return setResultSuccess();
		} else {
			return setResultError("保存失败");
		}
	}

	@RequestMapping("/remove/{id}")
	@ResponseBody()
	BaseResponse remove(@PathVariable Long id) {
		if (roleService.remove(id) > 0) {
			return setResultSuccess();
		} else {
			return setResultError("删除失败");
		}
	}

}
