package com.abing.permissionsys.controller;

import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysAppVO;
import com.abing.permissionsys.log.aop.Log;
import com.abing.permissionsys.service.AppService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 应用系统表
 *
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-06-21 10:54:03
 */

@RestController
@RequestMapping("/app")
@Slf4j
public class AppController extends BaseController{


	@Autowired
	private AppService appService;

	@Log("应用查询")
	@ResponseBody
	@RequestMapping(value = "/listPage")
	public BaseResponse listPage(SysAppVO appVO, PageVal pageVal){
		//查询列表数据
		PageResult appDOIPage=appService.list(appVO,pageVal);
		log.info("SysAppVO查询：{}",appDOIPage);
		return setResultSuccess(appDOIPage);
	}
	@ResponseBody
	@RequestMapping(value = "/listAll")
	public BaseResponse listAll(){
		//查询列表数据
		List<SysAppVO> sysAppVOList=appService.list();
		log.info("sysAppVOList查询：{}",sysAppVOList);
		return setResultSuccess(sysAppVOList);
	}
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	public BaseResponse save(@RequestBody SysAppVO app){
		if(appService.save(app)>0){
			return setResultSuccess();
		}
		return setResultError();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	public BaseResponse update(@RequestBody SysAppVO app){
		if(appService.update(app)>0){
			return setResultSuccess();
		}
		return setResultError();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	public BaseResponse remove(@RequestBody SysAppVO app){
		if(appService.remove(app.getId())>0){
		return setResultSuccess();
		}
		return setResultError();
	}

}
