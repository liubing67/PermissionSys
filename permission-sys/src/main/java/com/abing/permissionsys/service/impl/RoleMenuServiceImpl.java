package com.abing.permissionsys.service.impl;

import com.abing.permissionsys.entity.domain.SysRoleMenu;
import com.abing.permissionsys.mapper.RoleMenuMapper;
import com.abing.permissionsys.service.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
public class RoleMenuServiceImpl implements RoleMenuService {

    @Autowired
    RoleMenuMapper roleMenuMapper;
    @Override
    public List<Long> getMenuIdsByRoleId(Long roleId) {
        return roleMenuMapper.getMenuIdsByRoleId(roleId);
    }

    @Override
    @Transactional
    public void saveMenuByRoleId(String roleId, List<String> menuIds) {

        roleMenuMapper.removeMenusByRoleId(Long.parseLong(roleId));
        for (String menuId:menuIds){
            SysRoleMenu sysRoleMenu=new SysRoleMenu();
            sysRoleMenu.setRoleId(Long.parseLong(roleId));
            sysRoleMenu.setMenuId(Long.parseLong(menuId));
            roleMenuMapper.insert(sysRoleMenu);
        }
    }
}
