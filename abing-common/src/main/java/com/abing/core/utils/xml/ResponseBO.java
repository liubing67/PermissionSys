package com.abing.core.utils.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

//@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseBO<T> {
    @XmlElement(name = "head")
    private ResponseHeadBO head;

    @XmlElement(name = "body")
    private ResponseCardQueryBodyBO responseCardQueryBody;

    public ResponseHeadBO getHead() {
        return head;
    }

    public void setHead(ResponseHeadBO head) {
        this.head = head;
    }

    public ResponseCardQueryBodyBO getResponseCardQueryBody() {
        return responseCardQueryBody;
    }

    public void setResponseCardQueryBody(ResponseCardQueryBodyBO responseCardQueryBody) {
        this.responseCardQueryBody = responseCardQueryBody;
    }

    @Override
    public String toString() {
        return "ResponseBO [head=" + head + ", responseCardQueryBody=" + responseCardQueryBody + "]";
    }
}