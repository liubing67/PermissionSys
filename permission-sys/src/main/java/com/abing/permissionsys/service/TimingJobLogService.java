package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.TimingJobLogVO;

/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */
public interface TimingJobLogService {

	PageResult list(TimingJobLogVO timingLogVO, PageVal pageVal);

	int save(TimingJobLogVO timingLog);
	
	int update(TimingJobLogVO timingLog);
	
	int remove(Long id);
	
}
