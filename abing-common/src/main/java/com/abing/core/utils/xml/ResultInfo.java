package com.abing.core.utils.xml;

import lombok.Data;

@Data
public class ResultInfo {

    private String resultStatus;
    private String resultCode;
    private String resultMsg;
}
