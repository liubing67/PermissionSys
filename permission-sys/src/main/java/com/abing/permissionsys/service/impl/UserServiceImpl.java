package com.abing.permissionsys.service.impl;

import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.PageNavigation;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.domain.SysUser;
import com.abing.permissionsys.entity.vo.SysUserVO;
import com.abing.permissionsys.mapper.UserMapper;
import com.abing.permissionsys.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public SysUserVO selectOne(SysUserVO sysUserVO) {
        SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().setEntity(BeanUtil.voToDo(sysUserVO,SysUser.class)));
        return BeanUtil.doToVo(sysUser,SysUserVO.class);
    }

    @Override
    public PageResult list(SysUserVO userVO, PageVal pageVal) {
        QueryWrapper<SysUser> queryWrapper=new QueryWrapper<SysUser>();
        queryWrapper.eq(!StringUtils.isNullOrEmpty(userVO.getUsername()),"username",userVO.getUsername());
        queryWrapper.eq((null!=userVO.getDeptId())&&(0!=userVO.getDeptId()),"dept_id",userVO.getDeptId());
        IPage<SysUser> appDoList=userMapper.selectPage(new PageNavigation<>(pageVal),queryWrapper);
        List<SysUserVO> appVOS= BeanUtil.doToVoList(appDoList.getRecords(),SysUserVO.class);
        return new PageResult(appDoList,appVOS);
    }

    @Override
    public int save(SysUserVO sysUserVO) {
        return userMapper.insert(BeanUtil.voToDo(sysUserVO,SysUser.class));
    }

    @Override
    public int update(SysUserVO sysUserVO) {
        return userMapper.updateById(BeanUtil.voToDo(sysUserVO,SysUser.class));
    }

    @Override
    public int remove(Long userId) {
        return userMapper.deleteById(userId);
    }
}
