package com.abing.base;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 
 * 
 * 
 * @description:BaseDo
 */
@Data
public class BaseDo implements Serializable {

	/**
	 * id
	 */
	private Integer id;

	/**
	 * 是否可用 0可用 1不可用
	 */
	private Integer isAvailability;
}
