package com.abing.core.utils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.SAXValidator;
import org.dom4j.io.XMLWriter;
import org.dom4j.util.XMLErrorHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abing
 */
public class XMLUtils {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws DocumentException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "李四");
        map.put("age", 25);
        
        //System.out.println(map2xml(map));
        
        
        List<Object> list = new ArrayList<Object>();
        list.add("测试1");
        list.add("测试2");
        list.add("测试3");
        
        //System.out.println(list2xml(list,"items"));
        
        List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
        Map<String,Object> map1 = new HashMap<String,Object>();
        map1.put("name", "张三");
        map1.put("age", 23);
        listMap.add(map1);
        map1 = new HashMap<String,Object>();
        map1.put("name", "李四");
        map1.put("age", 24);
        listMap.add(map1);
        map1 = new HashMap<String,Object>();
        map1.put("name", "王五");
        map1.put("age", 25);
        listMap.add(map1);
        
        System.out.println(listMap2xml(listMap,"users","user"));

//        test4();
    }
    
    
    public static String listMap2xml(List<Map<String,Object>> list,String listRoot,String mapRoot){
        Document doc = DocumentHelper.createDocument();
        
        Element rootEle = doc.addElement("result");
        Element noEle = rootEle.addElement("no");
        Element msgEle = rootEle.addElement("msg");
        
        if(null!=list && !list.isEmpty()){
            noEle.setText("1");
            msgEle.setText("成功获取相关信息");
            
            Element listRootEle = rootEle.addElement(listRoot);
            
            for(Map<String,Object> map:list){
                
                Element mapRootELe = listRootEle.addElement(mapRoot);
                
                Set<Map.Entry<String,Object>> set = map.entrySet();
                Iterator<Map.Entry<String,Object>> iter = set.iterator();
                while(iter.hasNext()){
                    Map.Entry<String,Object> entry = (Map.Entry<String,Object>)iter.next();
                    
                    Element ele = mapRootELe.addElement(entry.getKey());
                    ele.setText(String.valueOf(entry.getValue()));
                }
            }
        }else{
            noEle.setText("0");
            msgEle.setText("没有获取到相关信息");
        }
        
        StringWriter sw = new StringWriter();
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("utf-8");
        
        try {
            
            XMLWriter xmlWriter = new XMLWriter(sw, format);
            
            xmlWriter.write(doc);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                sw.close();
            } catch (IOException ex) {
                Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return sw.toString();
    }

    
    public static String list2xml(List<Object> list,String itemRoot){
        Document doc = DocumentHelper.createDocument();
        
        Element rootEle = doc.addElement("result");
        Element noEle = rootEle.addElement("no");
        Element msgEle = rootEle.addElement("msg");
        
        if(null!=list && !list.isEmpty()){
            noEle.setText("1");
            msgEle.setText("成功获取相关信息");
            
            Element itemRootEle = rootEle.addElement(itemRoot);
            
            for(Object obj:list){
                Element ele = itemRootEle.addElement("item");
                ele.setText(String.valueOf(obj));
            }
        }else{
            noEle.setText("0");
            msgEle.setText("没有获取到相关信息");
        }
        
        StringWriter sw = new StringWriter();
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("utf-8");
        
        try {
            
            XMLWriter xmlWriter = new XMLWriter(sw, format);
            
            xmlWriter.write(doc);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                sw.close();
            } catch (IOException ex) {
                Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return sw.toString();
    }
    
    public static String map2xml(Map<String, Object> map) {
        Document doc = DocumentHelper.createDocument();
        
        Element rootEle = doc.addElement("result");
        
        Element noEle = rootEle.addElement("no");
        Element msgEle = rootEle.addElement("msg");
        
        if(null!=map && !map.isEmpty()){
            noEle.setText("1");
            msgEle.setText("成功获取相关信息");
            
            Set<Map.Entry<String, Object>> set = map.entrySet();
            Iterator<Map.Entry<String, Object>> iter = set.iterator();
            while(iter.hasNext()){
                Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iter.next();
                
                Element ele = rootEle.addElement(entry.getKey());
                ele.setText(String.valueOf(entry.getValue()));
            }
        }else{
            noEle.setText("0");
            msgEle.setText("没有获取到相关信息");
        }
        
        StringWriter sw = new StringWriter();
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("utf-8");
        
        try {
            
            XMLWriter xmlWriter = new XMLWriter(sw, format);
            
            xmlWriter.write(doc);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                sw.close();
            } catch (IOException ex) {
                Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return sw.toString();
    }
    
    


    public static void test4() {
        Document doc = DocumentHelper.createDocument();

        Element rootEle = doc.addElement("sudent");

        Element nameEle = rootEle.addElement("name");
        nameEle.setText("张三");

        Element ageEle = rootEle.addElement("age");
        ageEle.setText("25");

        try {
            StringWriter sw = new StringWriter();
            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setEncoding("utf-8");
            XMLWriter xmlWriter = new XMLWriter(sw, format);

            xmlWriter.write(doc);

            System.out.println(sw.toString());
        } catch (IOException ex) {
            Logger.getLogger(XMLUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void test3() throws DocumentException {
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<books>"
                + "    <book>"
                + "        <name>Think in Java</name>"
                + "        <price>120.0</price>"
                + "           <chapters>"
                + "               <c>001</c>"
                + "               <c>002</c>"
                + "               <c>003</c>"
                + "           </chapters>"
                + "    </book>"
                + "    <book>"
                + "        <name>Think in Java2</name>"
                + "        <price>220.0</price>"
                + "    </book>"
                + "</books>";
        Document doc = DocumentHelper.parseText(str);
        Element books = doc.getRootElement();
        List<Element> childEles = books.elements();
        Iterator<Element> iter = childEles.iterator();
        while (iter.hasNext()) {
            Element book = iter.next();

            Element name = book.element("name");
            Element price = book.element("price");

            System.out.println("name:" + name.getText() + ",price:" + price.getText());

            Element chapters = book.element("chapters");
            if (null != chapters) {
                Iterator<Element> chaptersIter = chapters.elementIterator();
                if (null != chaptersIter) {
                    while (chaptersIter.hasNext()) {
                        Element c = chaptersIter.next();
                        System.out.println("===>" + c.getText());
                    }
                }
            }

        }
    }

    public static void test2() throws DocumentException {
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<books>"
                + "    <book>"
                + "        <name>Think in Java</name>"
                + "        <price>120.0</price>"
                + "    </book>"
                + "    <book>"
                + "        <name>Think in Java2</name>"
                + "        <price>220.0</price>"
                + "    </book>"
                + "</books>";

        Document doc = DocumentHelper.parseText(str);

        Element books = doc.getRootElement();

        List<Element> childEles = books.elements();
        Iterator<Element> iter = childEles.iterator();
        while (iter.hasNext()) {
            Element book = iter.next();

            Element name = book.element("name");
            Element price = book.element("price");

            System.out.println("name:" + name.getText() + ",price:" + price.getText());
        }
    }

    public static void test1() throws DocumentException {
        String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<dzswdjz>"
                + "    <qr_code>"
                + "　　　<nsrsbh>nsrsbh</nsrsbh >"
                + "　　　<retStatus >retStatus</retStatus>"
                + "　　　<img_name>img_name</img_name>"
                + "      <img_byteString>img_byteString</img_byteString>"
                + "   </qr_code>"
                + "</dzswdjz>";
        Document doc = DocumentHelper.parseText(str);

        //获取到父节点
        Element dzswdjz = doc.getRootElement();

        //定位到qr_code节点
        Element qr_code = dzswdjz.element("qr_code");

        Element nsrsbh = qr_code.element("nsrsbh");
        Element retStatus = qr_code.element("retStatus");
        Element img_name = qr_code.element("img_name");
        Element img_byteString = qr_code.element("img_byteString");

        System.out.println("nsrsbh:" + nsrsbh.getText());
        System.out.println("retStatus:" + retStatus.getText());
        System.out.println("img_name:" + img_name.getText());
        System.out.println("img_byteString:" + img_byteString.getText());
    }



    /**
     * 通过XSD（XML Schema）校验XML
     */

    public static void validateXMLByXSD() {

        String xmlFileName = "D:\\xml.xml";

        String xsdFileName = "D:\\V1.2.0改动xsd\\pcac.ries.025.xsd";

        try {

            //创建默认的XML错误处理器

            XMLErrorHandler errorHandler = new XMLErrorHandler();

            //获取基于 SAX 的解析器的实例

            SAXParserFactory factory = SAXParserFactory.newInstance();

            //解析器在解析时验证 XML 内容。

            factory.setValidating(true);

            //指定由此代码生成的解析器将提供对 XML 名称空间的支持。

            factory.setNamespaceAware(true);

            //使用当前配置的工厂参数创建 SAXParser 的一个新实例。

            SAXParser parser = factory.newSAXParser();

            //创建一个读取工具

            SAXReader xmlReader = new SAXReader();

            //获取要校验xml文档实例

            Document xmlDocument = (Document) xmlReader.read(new File(xmlFileName));

            //设置 XMLReader 的基础实现中的特定属性。核心功能和属性列表可以在 http://sax.sourceforge.net/?selected=get-set 中找到。

            parser.setProperty(

                    "http://java.sun.com/xml/jaxp/properties/schemaLanguage",

                    "http://www.w3.org/2001/XMLSchema");

            parser.setProperty(

                    "http://java.sun.com/xml/jaxp/properties/schemaSource",

                    "file:" + xsdFileName);

            //创建一个SAXValidator校验工具，并设置校验工具的属性

            SAXValidator validator = new SAXValidator(parser.getXMLReader());

            //设置校验工具的错误处理器，当发生错误时，可以从处理器对象中得到错误信息。

            validator.setErrorHandler(errorHandler);

            //校验

            validator.validate(xmlDocument);

            XMLWriter writer = new XMLWriter(OutputFormat.createPrettyPrint());

            //如果错误信息不为空，说明校验失败，打印错误信息

            if (errorHandler.getErrors().hasContent()) {

                System.out.println("XML文件通过XSD文件校验失败！");

                writer.write(errorHandler.getErrors());

            } else {

                System.out.println("Good! XML文件通过XSD文件校验成功！");

            }

        } catch (Exception ex) {

            System.out.println("XML文件: " + xmlFileName + " 通过XSD文件:" + xsdFileName + "检验失败。\n原因： " + ex.getMessage());

            ex.printStackTrace();

        }
    }
}