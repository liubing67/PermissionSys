package com.abing.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

//public class UserFilter implements Filter {
//    private static final Logger LOGGER = LoggerFactory.getLogger(UserFilter.class);
//
//    @Value("${isMultiLogin}")
//    private String isMultiLogin;
//
////    @Autowired
////    private JwtUtils jwtUtils;
//
////    @Autowired
////    private BaseManager baseManager;
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//
//    }
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//
//        //排除特定URL
//        List<String> allowedPaths = new ArrayList<>(Arrays.asList(
//        		"abc/login"
//        		));
//        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
//        LOGGER.info("allowedPaths->{},path->{}", allowedPaths, path);
//        boolean allowedPath = contains(allowedPaths,path);
//        if (allowedPath) {
//            filterChain.doFilter(servletRequest, servletResponse);
//            return;
//        }
//
//        String authorization = request.getHeader("Authorization");
//        LOGGER.info("获取到的请求头:{}", authorization);
//        if (StringUtils.isEmpty(authorization)) {
//            LOGGER.info("authorization为空");
//            BaseResult errorResponse = new BaseResult(ReturnCode.TokenIsNull.getCode(), ReturnCode.TokenIsNull.getMsg());
//            convertObjectToJson(errorResponse,servletResponse);
//            return;
//        }
//        String token = authorization.replace("Bearer", "").trim();
//        if (checkToken(token)) {
////            UserRequest userRequest = jwtUtils.getUserRequest(token);
//            UserInfoRes userRequest = jwtUtils.getUserRequest(token);
//            ParameterRequestWrapper parameterRequestWrapper = new ParameterRequestWrapper(request);
//            parameterRequestWrapper.addObject(userRequest);
//            filterChain.doFilter(parameterRequestWrapper, servletResponse);
//            return;
//        }
//        BaseResult baseResult = new BaseResult(ReturnCode.ValidTokenError.getCode(), ReturnCode.ValidTokenError.getMsg());
//        convertObjectToJson(baseResult,servletResponse);
//    }
//
//    @Override
//    public void destroy() {
//
//    }
//
//    private boolean checkToken(String token) {
//        if (StringUtils.isEmpty(token)) {
//            LOGGER.info("token为空");
//            return false;
//        }
//        LOGGER.info("checkToken#token->{},isMultiLogin->{}", token, isMultiLogin);
//        return baseManager.verifyToken(token);
//    }
//    public boolean contains(List list,String o) {
//        Iterator<String> it = list.iterator();
//        if (o==null) {
//            while (it.hasNext())
//                if (it.next()==null)
//                    return true;
//        } else {
//            while (it.hasNext())
//                if (it.next().contains(o))
//                    return true;
//        }
//        return false;
//    }
//
//    /**
//     * 返回json格式数据
//     * @param errorResponse
//     * @param servletResponse
//     * @throws IOException
//     *
//     * {
//     *     "code": "1901",
//     *     "msg": "Token验证不通过"
//     * }
//     */
//    public void convertObjectToJson(BaseResult errorResponse, ServletResponse servletResponse) throws IOException {
//        servletResponse.setContentType("application/json; charset=utf-8");
//        servletResponse.setCharacterEncoding("UTF-8");
//        ObjectMapper mapper = new ObjectMapper();
//        String userJson = mapper.writeValueAsString(errorResponse);
//        OutputStream out = servletResponse.getOutputStream();
//        out.write(userJson.getBytes("UTF-8"));
//        out.flush();
//    }
//}
