package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


/**
 *
 * @author liubing
 * @date 2017-10-03 09:45:11
 */
@Mapper
public interface UserMapper extends BaseMapper<SysUser> {



}
