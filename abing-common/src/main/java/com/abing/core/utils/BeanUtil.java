package com.abing.core.utils;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * 
 * 
 * 
 * @description:dto转换DO工具类 do转换DTO
 */
public class BeanUtil {

	/**
	 * dot 转换为Do 工具类
	 * 
	 * @param dtoEntity
	 * @param doClass
	 * @return
	 */
	public static <Do> Do dtoToDo(Object dtoEntity, Class<Do> doClass) {
		// 判断dto是否为空!
		if (dtoEntity == null) {
			return null;
		}
		// 判断DoClass 是否为空
		if (doClass == null) {
			return null;
		}
		try {
			Do newInstance = doClass.newInstance();
			org.springframework.beans.BeanUtils.copyProperties(dtoEntity, newInstance);
			// Dto转换Do
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * do 转换为Dto 工具类
	 * 
	 * @param doEntity
	 * @param dtoClass
	 * @return
	 */
	public static <Dto> Dto doToDto(Object doEntity, Class<Dto> dtoClass) {
		// 判断dto是否为空!
		if (doEntity == null) {
			return null;
		}
		// 判断DoClass 是否为空
		if (dtoClass == null) {
			return null;
		}
		try {
			Dto newInstance = dtoClass.newInstance();
			org.springframework.beans.BeanUtils.copyProperties(doEntity, newInstance);
			// Dto转换Do
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}
	// 后面集合类型带封装
	/**
	 * vo 转换为Dto 工具类
	 *
	 * @param voEntity
	 * @param dtoClass
	 * @return
	 */
	public static <Dto> Dto voToDto(Object voEntity, Class<Dto> dtoClass) {
		// 判断VoSF是否为空!
		if (voEntity == null) {
			return null;
		}
		// 判断DtoClass 是否为空
		if (dtoClass == null) {
			return null;
		}
		try {
			Dto newInstance = dtoClass.newInstance();
			org.springframework.beans.BeanUtils.copyProperties(voEntity, newInstance);
			// Dto转换Do
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * vo 转换为Do 工具类
	 *
	 * @param voEntity
	 * @param doClass
	 * @return
	 */
	public static <Do> Do voToDo(Object voEntity, Class<Do> doClass) {
		// 判断VoSF是否为空!
		if (voEntity == null) {
			return null;
		}
		// 判断DtoClass 是否为空
		if (doClass == null) {
			return null;
		}
		try {
			Do newInstance = doClass.newInstance();
			org.springframework.beans.BeanUtils.copyProperties(voEntity, newInstance);
			// vo转换Do
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * do 转换为vo 工具类
	 *
	 * @param doEntity
	 * @param voClass
	 * @return
	 */
	public static <Vo> Vo doToVo(Object doEntity, Class<Vo> voClass) {
		// 判断VoSF是否为空!
		if (doEntity == null) {
			return null;
		}
		// 判断DtoClass 是否为空
		if (voClass == null) {
			return null;
		}
		try {
			Vo newInstance = voClass.newInstance();
			org.springframework.beans.BeanUtils.copyProperties(doEntity, newInstance);
			// Dto转换Do
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * do 转换为vo集合 工具类
	 *
	 * @param doEntity
	 * @param voClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <Vo> List<Vo> doToVoList(List doEntity, Class<Vo> voClass) {
		// 判断VoSF是否为空!
		if (doEntity == null) {
			return null;
		}
		// 判断DtoClass 是否为空
		if (voClass == null) {
			return null;
		}
		try {
			MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
			List<Vo> mapAsList = mapperFactory.getMapperFacade().mapAsList(doEntity, voClass);
			return mapAsList;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * do 转换为vo 工具类
	 *
	 * @param source
	 * @param destinationClass
	 * @return
	 */
	public static <T> T change(Object source, Class<T> destinationClass) {
		// 判断VoSF是否为空!
		if (source == null) {
			return null;
		}
		// 判断DtoClass 是否为空
		if (destinationClass == null) {
			return null;
		}
		try {
			T newInstance = destinationClass.newInstance();
			BeanUtils.copyProperties(source, newInstance);
			// Dto转换Do
			return newInstance;
		} catch (Exception e) {
			return null;
		}
	}
}
