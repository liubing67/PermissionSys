package com.abing.core.utils.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDocumentBO<T> {

    @XmlElement(name = "response")
    private ResponseBO<T> response;

    @XmlElement(name = "signature")
    private String signature;

    public ResponseBO<T> getResponse() {
        return response;
    }

    public void setResponse(ResponseBO<T> response) {
        this.response = response;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }


    @Override
    public String toString() {
        return "ResponseDocumentBO{" +
                "response=" + response +
                ", signature='" + signature + '\'' +
                '}';
    }
}