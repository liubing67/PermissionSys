package com.abing.permissionsys.service.impl;

import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.domain.SysMenu;
import com.abing.permissionsys.entity.vo.SysMenuVO;
import com.abing.permissionsys.entity.vo.menu.MenuVo;
import com.abing.permissionsys.mapper.MenuMapper;
import com.abing.permissionsys.service.MenuService;
import com.abing.permissionsys.utils.BuildTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class MenuServiceImpl implements MenuService {
	@Autowired
    MenuMapper menuMapper;

	@Transactional(readOnly = false,rollbackFor = Exception.class)
	@Override
	public int remove(Long id) {
		int result = menuMapper.deleteById(id);
		return result;
	}
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	@Override
	public int save(SysMenuVO menu) {
		int r = menuMapper.insert(BeanUtil.voToDo(menu,SysMenu.class));
		return r;
	}

	@Transactional(readOnly = false,rollbackFor = Exception.class)
	@Override
	public int update(SysMenuVO menu) {
		int r = menuMapper.updateById(BeanUtil.voToDo(menu,SysMenu.class));
		return r;
	}

	@Override
	public List<String> listPerms(Long userId) {
		List<String> perms = menuMapper.listUserPerms(userId);
		return perms;
	}

	@Override
	public List<MenuVo> getUserMenuByAppId(Long userId, Long appId) {
		List<SysMenu> menuDOs = menuMapper.listMenuByUserId(userId,appId);
		// 默认顶级菜单为０，根据数据库实际情况调整
		List<MenuVo> list = BuildTree.buildLists(BeanUtil.doToVoList(menuDOs,SysMenuVO.class));
		return list;
	}

	@Override
	public List<SysMenuVO> getMenuByAppId(Long appId) {
		List<SysMenu> sysMenus = menuMapper.listMenuByAppId(appId);
		List<SysMenuVO> sysMenuVOS = new ArrayList<SysMenuVO>();
		for (SysMenu sysMenu : sysMenus) {
			Long mid = sysMenu.getId();
			Long pid = sysMenu.getParentId();
			if (0 != pid) {
				continue;
			}
			if (pid == null || 0 == pid) {
				SysMenuVO sysMenuVO=BeanUtil.doToVo(sysMenu,SysMenuVO.class);
				sysMenuVO.setChildren(getListTreeSysMenu(sysMenus,mid));
				sysMenuVOS.add(sysMenuVO);
			}
		}

		//设置顶级节点
		SysMenuVO sysMenuVO=new SysMenuVO();
		if (sysMenuVOS.size() == 1) {
			sysMenuVO = sysMenuVOS.get(0);
		} else {
			sysMenuVO.setId(0l);
			sysMenuVO.setParentId(0l);
			sysMenuVO.setChildren(sysMenuVOS);
			sysMenuVO.setName("顶级节点");
		}
		List<SysMenuVO> list=new ArrayList<SysMenuVO>();
		list.add(sysMenuVO);
		return list;
	}

	private List<SysMenuVO> getListTreeSysMenu(List<SysMenu> sysMenus,Long ppid){
		List<SysMenuVO> sysMenuVOS = new ArrayList<SysMenuVO>();
		for (SysMenu sysMenu : sysMenus) {
			Long mid = sysMenu.getId();
			Long pid = sysMenu.getParentId();
			if (pid != null && pid == ppid) {
				SysMenuVO sysMenuVO=BeanUtil.doToVo(sysMenu,SysMenuVO.class);
				sysMenuVO.setChildren(getListTreeSysMenu(sysMenus,mid));
				sysMenuVOS.add(sysMenuVO);
			}
		}
		return sysMenuVOS;
	}
}
