package com.abing.permissionsys.service.impl;

import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.domain.SysDept;
import com.abing.permissionsys.entity.vo.SysDeptVO;
import com.abing.permissionsys.entity.vo.dept.DeptVO;
import com.abing.permissionsys.mapper.DeptMapper;
import com.abing.permissionsys.service.DeptService;
import com.abing.permissionsys.utils.BuildTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper sysDeptMapper;

    @Override
    public int count(Map<String, Object> map) {
        return sysDeptMapper.count(map);
    }

    @Override
    public int save(SysDeptVO sysDept) {
        return sysDeptMapper.insert(BeanUtil.voToDo(sysDept,SysDept.class));
    }

    @Override
    public int update(SysDeptVO sysDept) {
        return sysDeptMapper.updateById(BeanUtil.voToDo(sysDept,SysDept.class));
    }

    @Override
    public int remove(Long deptId) {
        return sysDeptMapper.deleteById(deptId);
    }

    @Override
    public List<DeptVO> getTree(SysDeptVO sysDeptVO) {
        List<SysDept> sysDepts = sysDeptMapper.list(BeanUtil.voToDo(sysDeptVO,SysDept.class));
        // 默认顶级菜单为０，根据数据库实际情况调整
        DeptVO t = BuildTree.buildDept(BeanUtil.doToVoList(sysDepts,SysDeptVO.class));
        List<DeptVO> list=new ArrayList<>();
        list.add(t);
        return list;
    }

    @Override
    public boolean checkDeptHasUser(Long deptId) {
        // TODO Auto-generated method stub
        //查询部门以及此部门的下级部门
        int result = sysDeptMapper.getDeptUserNumber(deptId);
        return result == 0 ? true : false;
    }

}
