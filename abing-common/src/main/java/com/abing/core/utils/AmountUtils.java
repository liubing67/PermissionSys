package com.abing.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**  
 * com.util.AmountUtils  
 * @description  金额元分之间转换工具类  
 */
public class AmountUtils {    
        
    /**金额为分的格式 */    
    public static final String CURRENCY_FEN_REGEX = "\\-?[0-9]+";    
    public final static BigDecimal BigDecimal1000 = new BigDecimal("1000");
    public final static BigDecimal BigDecimal100 = new BigDecimal("100");
    public final static BigDecimal BigDecimal1 = new BigDecimal("1");
    public final static BigDecimal BigDecimal0 = new BigDecimal("0");
    public final static  DecimalFormat df = new DecimalFormat("#,###.00");     
    /**   
     * 将分为单位的转换为元并返回金额格式的字符串 （除100）  
     *   
     * @param amount  
     * @return  
     * @throws Exception   
     */    
    public static String changeF2Y(Long amount) throws Exception{    
        if(!amount.toString().matches(CURRENCY_FEN_REGEX)) {    
            throw new Exception("金额格式有误");    
        }    
            
        int flag = 0;    
        String amString = amount.toString();    
        if(amString.charAt(0)=='-'){    
            flag = 1;    
            amString = amString.substring(1);    
        }    
        StringBuffer result = new StringBuffer();    
        if(amString.length()==1){    
            result.append("0.0").append(amString);    
        }else if(amString.length() == 2){    
            result.append("0.").append(amString);    
        }else{    
            String intString = amString.substring(0,amString.length()-2);    
            for(int i=1; i<=intString.length();i++){    
                if( (i-1)%3 == 0 && i !=1){    
                    result.append(",");    
                }    
                result.append(intString.substring(intString.length()-i,intString.length()-i+1));    
            }    
            result.reverse().append(".").append(amString.substring(amString.length()-2));    
        }    
        if(flag == 1){    
            return "-"+result.toString();    
        }else{    
            return result.toString();    
        }    
    }    
    /**
     * @Description:
     * @param amount 金额  分
     * @param formatStr 格式化字符串——"#,###.00" 保留两位小数  每三位加逗号隔离;"#,###"——只保留整数 ; "#,###.000"——三位小数 ; "###.000"——三位小数
     * @return 格式化后的金额  单位元
     * @throws Exception String
     * @Date: 2019年1月29日 下午4:38:29
     */
    public static String changeF2YAndFormat(BigDecimal amount,String formatStr) throws Exception{
    	
    	if(amount==null){
			return "0";
		}else if(amount.intValue()==0){
			return "0";
		}else{
			//分转元
			amount=amount.divide(BigDecimal100, 2,BigDecimal.ROUND_HALF_UP);
			if(BigDecimal1.compareTo(amount)==-1){
				//格式化
				DecimalFormat decimalFormat=null;
				if(StringUtils.isBlank(formatStr)){
					decimalFormat = df; 
				}else{
					decimalFormat = new DecimalFormat(formatStr); 
				}
				return decimalFormat.format(amount.doubleValue());
			}
			
			return String.valueOf(amount);
		}
    }    
        
    /**
     * @Description:将String分转换为String元 （除100）   把输入的str分小数点后面的切除（如果有）
     * @param amount String
     * @return String
     * @throws Exception String
     * @Date: 2018年7月12日 上午10:56:57
     */
    public static String changeF2Y(String amount) throws Exception{ 
    	//把小数点后面的切除
    	String[] split = amount.split("\\.");
    	if(split.length>1){
    		amount=split[0];
    	}
        if(!amount.matches(CURRENCY_FEN_REGEX)) {    
            throw new Exception("金额格式有误");    
        }    
        return BigDecimal.valueOf(Long.valueOf(amount)).divide(new BigDecimal(100)).toString();    
    }    
    
    /**
     * @Description:将String分转换为String元 （除100）   把输入的str分小数点后面的切除（如果有）  并把元格式化为两位小数点
     * @param amount
     * @return
     * @throws Exception String
     * @Date: 2018年9月7日 下午8:02:13
     */
    public static String changeF2YFormat(String amount) throws Exception{ 
    	//把小数点后面的切除
    	String[] split = amount.split(".");
    	if(split.length>1){
    		amount=split[0];
    	}
    	if(!amount.matches(CURRENCY_FEN_REGEX)) {    
    		throw new Exception("金额格式有误");    
    	}  
    	
    	String yua = BigDecimal.valueOf(Long.valueOf(amount)).divide(new BigDecimal(100)).toString();
		String[] yuaSplit = yua.split("\\.");
		if(yuaSplit.length==1){
			yua=yua+".00";
		}else if(yuaSplit[1].length()<2){
			yua=yua+"0";
		}
		
    	return yua;    
    }    
   
        
    /**   
     * 将元为单位的转换为分 （乘100）  
     *   
     * @param amount  String
     * @return  String
     */    
    public static String changeY2F(Long amount){    
        return BigDecimal.valueOf(amount).multiply(new BigDecimal(100)).toString();    
    } 
     
    /**
     * @Description:将(BigDecimal)分为单位的转换为(String)元 （除100）
     * @param bdFen BigDecimal分
     * @return  String元
     * @throws Exception String
     * @Date: 2018年7月12日 上午10:58:24
     */
    public static String getStrYuanbyBdFen(BigDecimal bdFen) throws Exception{  
    	String amount = bdFen.toString();
    	if(!amount.matches(CURRENCY_FEN_REGEX)) {    
    		throw new Exception("金额格式有误");    
    	}    
    	return bdFen.divide(new BigDecimal(100)).toString();    
    } 
 
    /**
     * @Description:将(String)元为单位的转换为(BigDecimal)分 （乘100）
     * @param strYuan String元
     * @return  BigDecimal分
     * @throws Exception BigDecimal
     * @Date: 2018年7月12日 上午10:59:04
     */
    public static BigDecimal getBdFenbyStrYuan(String strYuan) throws Exception{ 
    	if(StringUtils.isBlank(strYuan)) {
    		throw new Exception("金额格式有误");    
    	}   
    	
    	BigDecimal bdYuan = new BigDecimal(strYuan);
    	bdYuan = bdYuan.multiply(new BigDecimal(100));
    	
    	return bdYuan;    
    }    
        
    /**   
     * 将元为单位的转换为分 替换小数点，支持以逗号区分的金额  
     *   
     * @param amount  
     * @return  
     */    
    public static String changeY2F(String amount){    
        String currency =  amount.replaceAll("\\$|\\￥|\\,", "");  //处理包含, ￥ 或者$的金额    
        int index = currency.indexOf(".");    
        int length = currency.length();    
        Long amLong = 0l;    
        if(index == -1){    
            amLong = Long.valueOf(currency+"00");    
        }else if(length - index >= 3){    
            amLong = Long.valueOf((currency.substring(0, index+3)).replace(".", ""));    
        }else if(length - index == 2){    
            amLong = Long.valueOf((currency.substring(0, index+2)).replace(".", "")+0);    
        }else{    
            amLong = Long.valueOf((currency.substring(0, index+1)).replace(".", "")+"00");    
        }    
        return amLong.toString();    
    }    
        
   /* public static void main(String[] args) throws Exception {    
    	
		System.out.println(changeF2YFormat("1"));
		System.out.println(changeF2YFormat("10"));
		System.out.println(changeF2YFormat("100"));

            
    }*/ 
    /**
     * @Description:得出A*B的Int类型结果   不会丢失精度（强转除外）
     * @param A
     * @param B
     * @return
     * @throws Exception int
     * @Date: 2018年9月12日 下午2:49:00
     */
    public static int getIntMultiplyStrAB(String A,String B) throws Exception{
    	BigDecimal bigDecimalA = newBigDecimal(A);
    	BigDecimal bigDecimalB = newBigDecimal(B);
    	BigDecimal multiply = bigDecimalA.multiply(bigDecimalB);
    	int value = multiply.intValue();  
    	return value;    
    }         
    /**
     * @Description:得出A*B的String类型结果   不会丢失精度，不使用科学计数法
     * @param A
     * @param B
     * @return
     * @throws Exception String
     * @Date: 2018年9月12日 下午2:18:15
     */
    public static String getStrMultiplyStrAB(String A,String B) throws Exception{
    	BigDecimal bigDecimalA = newBigDecimal(A);
    	BigDecimal bigDecimalB = newBigDecimal(B);
    	BigDecimal multiply = bigDecimalA.multiply(bigDecimalB);
    	String value = multiply.toPlainString();  
    	return value;    
    }         
    public static BigDecimal getBdMultiplyStrAB(String A,String B) throws Exception{
    	BigDecimal bigDecimalA = newBigDecimal(A);
    	BigDecimal bigDecimalB = newBigDecimal(B);
    	BigDecimal value = bigDecimalA.multiply(bigDecimalB);
    	return value;    
    }

	private static BigDecimal newBigDecimal(String Str) throws Exception {
		BigDecimal bigDecimal;
    	try {
    		bigDecimal = new BigDecimal(Str);
    	} catch (Exception e) {
    		throw new Exception("传入的参数——"+Str+"——newBigDecimal报错"); 
    	}
		return bigDecimal;
	}         
  /* public static void main(String[] args) throws Exception {    
    	
		System.out.println(changeF2YFormat("1"));
		System.out.println(changeF2YFormat("10"));
		System.out.println(changeF2YFormat("100"));
		BigDecimal bd = new BigDecimal("1.238761976E-10");  
		System.out.println(bd.toPlainString());
		System.out.println(bd.toString());
		System.err.println(AmountUtils.getIntMultiplyStrAB("0.29","100"));
		System.err.println(AmountUtils.getBdMultiplyStrAB("0.29","100").intValue());
		System.err.println(0.29F*100L);
		System.err.println(0.29F*100F);
		System.err.println(0.29F*100D);
		float a=(float) (0.29D*100F);
		System.err.println(a);
            
    }  */ 
} 