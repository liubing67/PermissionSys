package com.abing.permissionsys.enums;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */

public class TimingJobEnums implements Serializable {

	public enum StateEnum{

		NORMAL("0","正常"),
		PAUSE("1","暂停");
		private String index;
		private String name;


		StateEnum(String index,String name){
			this.index=index;
			this.name=name;
		}
		public static String getName(String index) {
			for (StateEnum code : StateEnum.values()) {
				if (code.getIndex().equals(index)) {
					return code.name;
				}
			}
			return null;
		}

		public static String getIndex(String name) {
			for (StateEnum code : StateEnum.values()) {
				if (code.getName().equals(name)) {
					return code.index;
				}
			}
			return null;
		}

		// get set 方法
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getIndex() {
			return index;
		}

		public void setIndex(String index) {
			this.index = index;
		}
	}

}
