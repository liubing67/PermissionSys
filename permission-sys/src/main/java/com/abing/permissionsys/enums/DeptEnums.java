package com.abing.permissionsys.enums;

public class DeptEnums {


    public enum DeptFlagEnum{

        DeptFlagEnum_0("0","正常"),
        DeptFlagEnum_1("1","删除");
        private String index;
        private String name;


         DeptFlagEnum(String index,String name){
            this.index=index;
            this.name=name;
        }
        public static String getName(String index) {
            for (DeptFlagEnum code : DeptFlagEnum.values()) {
                if (code.getIndex().equals(index)) {
                    return code.name;
                }
            }
            return null;
        }

        public static String getIndex(String name) {
            for (DeptFlagEnum code : DeptFlagEnum.values()) {
                if (code.getName().equals(name)) {
                    return code.index;
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }
    }
}
