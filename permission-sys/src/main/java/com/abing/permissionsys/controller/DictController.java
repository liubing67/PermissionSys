package com.abing.permissionsys.controller;

import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.vo.ComboBoxVO;
import com.abing.permissionsys.entity.vo.SysDeptVO;
import com.abing.permissionsys.entity.vo.dept.DeptVO;
import com.abing.permissionsys.enums.DeptEnums;
import com.abing.permissionsys.enums.MenuEnums;
import com.abing.permissionsys.enums.UserEnums;
import com.abing.permissionsys.service.DeptService;
import com.abing.permissionsys.utils.DictionaryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 部门管理
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-27 14:40:36
 */

@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {

	/**
	 * 枚举
	 */
	@RequestMapping(value = "/initEnumList")
    BaseResponse initEnumList() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("del_flag", DictionaryUtils.putEnum(DeptEnums.DeptFlagEnum.values()));
        map.put("menuType", DictionaryUtils.putEnum(MenuEnums.MenuTypeEnum.values()));
        map.put("userStatus", DictionaryUtils.putEnum(UserEnums.UserStatusEnum.values()));
        return setResultSuccess(map);
    }

}
