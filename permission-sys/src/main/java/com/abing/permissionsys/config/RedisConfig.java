package com.abing.permissionsys.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.MapPropertySource;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashMap;
import java.util.Map;

@Data
@Configuration
@Slf4j
public class RedisConfig
{


//    @Bean
//    public JedisPoolConfig jedisPoolConfig()
//    {
//        JedisPoolConfig poolConfig = new JedisPoolConfig();
//        poolConfig.setMaxIdle(disconf.getMaxIdle());
//        poolConfig.setMaxTotal(disconf.getMaxTotal());
//        poolConfig.setMaxWaitMillis(disconf.getMaxWait());
//        poolConfig.setMinIdle(disconf.getMinIdle());
//        poolConfig.setTestOnBorrow(disconf.isTestOnBorrow());
//        return poolConfig;
//    }
//
//    @Bean
//    public RedisConnectionFactory jedisConnectionFactory(JedisPoolConfig poolConfig)
//    {
//        JedisConnectionFactory connectionFactory = new JedisConnectionFactory();
//        if(disconf.isCluster())
//        {
//            connectionFactory = new JedisConnectionFactory(redisClusterConfiguration(), poolConfig);
//        }
//        else
//        {
//            connectionFactory.setHostName(disconf.getHost());
//            connectionFactory.setPort(disconf.getPort());
//            connectionFactory.setPoolConfig(poolConfig);
//        }
//        connectionFactory.setPassword(disconf.getPassword());
//        return connectionFactory;
//    }

    /**
     * 原型模式
     * 主要是为了对K或者V指定不同的序列化方式
     * （如果是单例模式，再存放不同类型的数据，反序列化是报错）
     * @CreateDate 2018年6月22日上午11:25:32
     */
    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public <K, V> RedisTemplate<K, V> redisTemplate(RedisConnectionFactory redisConnectionFactory)
    {
        RedisTemplate<K, V> redisTemplate = new RedisTemplate<K, V>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // return redisTemplate;
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // key采用String的序列化方式
        redisTemplate.setKeySerializer(stringRedisSerializer);
        // hash的key也采用String的序列化方式
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用jackson
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value序列化方式采用jackson
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

//    private RedisClusterConfiguration redisClusterConfiguration()
//    {
//        Map<String, Object> source = new HashMap<String, Object>();
//        source.put("spring.redis.cluster.nodes", disconf.getClusterNodes());
//        source.put("spring.redis.cluster.timeout", disconf.getTimeout());
//        RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration(new MapPropertySource("RedisClusterConfiguration", source));
//        log.info("redis 集群方式连接. source = {}", source);
//        return redisClusterConfiguration;
//    }
}
