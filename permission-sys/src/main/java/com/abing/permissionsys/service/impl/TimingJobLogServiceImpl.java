package com.abing.permissionsys.service.impl;


import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.PageNavigation;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.domain.TimingJobLog;
import com.abing.permissionsys.entity.vo.TimingJobLogVO;
import com.abing.permissionsys.mapper.TimingJobLogMapper;
import com.abing.permissionsys.service.TimingJobLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TimingJobLogServiceImpl implements TimingJobLogService {
	@Autowired
	private TimingJobLogMapper timingLogMapper;
	
	@Override
	public PageResult list(TimingJobLogVO timingLogVO, PageVal pageVal) {
		QueryWrapper<TimingJobLog> queryWrapper=new QueryWrapper<TimingJobLog>();
		queryWrapper.eq(!StringUtils.isNullOrEmpty(timingLogVO.getClassName()),"class_name",timingLogVO.getClassName());
		queryWrapper.eq(!StringUtils.isNullOrEmpty(timingLogVO.getState()),"state",timingLogVO.getState());
		queryWrapper.ge(null!=timingLogVO.getCreateTimeStart(),"create_time",timingLogVO.getCreateTimeStart())
				.le(null!=timingLogVO.getCreateTimeEnd(),"create_time",timingLogVO.getCreateTimeEnd());
		IPage<TimingJobLog> appDoList=timingLogMapper.selectPage(new PageNavigation<>(pageVal),queryWrapper);
		List<TimingJobLogVO> timingJobLogVOS= BeanUtil.doToVoList(appDoList.getRecords(),TimingJobLogVO.class);
		return new PageResult(appDoList,timingJobLogVOS);
	}

	@Override
	public int save(TimingJobLogVO timingLog){
		return timingLogMapper.insert(BeanUtil.voToDo(timingLog, TimingJobLog.class));
	}
	
	@Override
	public int update(TimingJobLogVO timingLog){
		return timingLogMapper.updateById(BeanUtil.voToDo(timingLog, TimingJobLog.class));
	}
	
	@Override
	public int remove(Long id){
		return timingLogMapper.deleteById(id);
	}

}
