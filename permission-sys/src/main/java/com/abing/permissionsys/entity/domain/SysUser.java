package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	@TableId(value = "user_id",type = IdType.AUTO)
	private Long userId;
	//用户名
	private String username;
	//
	private String name;
	//密码
	private String password;
	//
	private Long deptId;
	//邮箱
	private String email;
	//手机号
	private String mobile;
	//状态 0:禁用，1:正常
	private Integer status;
	//创建用户id
	private Long userIdCreate;
	//创建时间
	private Date gmtCreate;
	//修改时间
	private Date gmtModified;
	//性别
	private Long sex;
	//出身日期
	private Date birth;
	//
	private Long picId;
	//现居住地
	private String liveAddress;
	//爱好
	private String hobby;
	//省份
	private String province;
	//所在城市
	private String city;
	//所在地区
	private String district;

}
