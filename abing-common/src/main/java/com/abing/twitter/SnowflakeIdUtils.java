package com.abing.twitter;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.IdcardUtil;

/**
 *
 *
 *
 * @description: 使用雪花算法生成全局id
 */
public class SnowflakeIdUtils {
	private static SnowflakeIdWorker idWorker;
	static {
		// 使用静态代码块初始化 SnowflakeIdWorker
		idWorker = new SnowflakeIdWorker(1, 1);
	}

	public static String nextId() {
		return idWorker.nextId() + "";
	}

	public static void main(String[] args) {
		for (int i = 0 ; i < 1000 ; i ++) {
			System.out.println(nextId());
			System.out.println(IdUtil.getSnowflake(1,1).nextId());
			System.out.println(IdUtil.getSnowflake(1,2).nextId());
		}
	}
}
