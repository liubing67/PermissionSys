package com.abing.exception;

import com.abing.constants.HttpResCode;

public class CustomerErrorException extends RuntimeException {

    private int code = HttpResCode.HttpResCode_500.getCode();
    public static final CustomerErrorException PHONE_ERROR = new CustomerErrorException(10011002, "手机号码不能为空！！");
    public CustomerErrorException() {
        super();
    }

    public CustomerErrorException(String message) {
        super(message);
    }

    public CustomerErrorException(int code,String message) {
        super(message);
        this.code = code;
    }

    public CustomerErrorException(int code,String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public CustomerErrorException(String message, Throwable cause) {
        super(message, cause);
    }
    public int getCode() {
        return code;
    }
}
