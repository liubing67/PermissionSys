package com.abing.permissionsys.timing.task;

import com.abing.permissionsys.entity.vo.SysLogVO;
import com.abing.permissionsys.service.LogService;
import com.abing.permissionsys.timing.BaseJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class LogTask implements BaseJob {
    @Autowired
    private LogService logService;

    @Override
    public void run() {

        SysLogVO sysLogVO=new SysLogVO();
        sysLogVO.setOperation("test");
        sysLogVO.setLogType("0");
        sysLogVO.setAddress("内网IP");
        sysLogVO.setBrowser("Chrome");
        sysLogVO.setGmtCreate(new Date());
        sysLogVO.setIp("0.0.0.0.0");
        sysLogVO.setMethod("com.abing.permissionsys.timing.task.LogTask.run()");
        sysLogVO.setParams("test");
        sysLogVO.setTime(1l);
        sysLogVO.setUsername("test");

        int i=logService.save(sysLogVO);
        log.info("执行成功--------------"+i);
    }
}
