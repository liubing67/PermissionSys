package com.abing.permissionsys.entity.vo.dept;

import com.abing.permissionsys.entity.vo.menu.MenuVo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 部门管理
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:26
 */
@Data
public class DeptVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//上级部门ID，一级部门为0
	private Long parentId;
	//部门名称
	private String name;
	//排序
	private Integer orderNum;
	//是否删除  -1：已删除  0：正常
	private Integer delFlag;
	private List<DeptVO> children;
}
