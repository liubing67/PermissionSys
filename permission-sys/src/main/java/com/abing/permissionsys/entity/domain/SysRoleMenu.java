package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 角色与菜单对应关系
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysRoleMenu implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;
	//角色ID
	private Long roleId;
	//菜单ID
	private Long menuId;

}
