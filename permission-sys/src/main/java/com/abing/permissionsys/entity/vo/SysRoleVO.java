package com.abing.permissionsys.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 角色
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysRoleVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long roleId;
	//角色名称
	private String roleName;
	//角色标识
	private String roleSign;
	//备注
	private String remark;
	//创建用户id
	private Long userIdCreate;
	//创建时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date gmtCreate;
	//创建时间
	private Date gmtModified;

}
