package com.abing.permissionsys.log.aspect;

import com.abing.core.utils.RequestHolder;
import com.abing.core.utils.ThrowableUtil;
import com.abing.permissionsys.entity.vo.SysLogVO;
import com.abing.permissionsys.enums.SysLogEnums;
import com.abing.permissionsys.service.LogService;
import com.abing.permissionsys.utils.SecurityUtils;
import com.abing.permissionsys.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @date 2018-11-24
 */
@Component
@Aspect
@Slf4j
public class LogAspect {

    private final LogService logService;

    private long currentTime = 0L;

    public LogAspect(LogService logService) {
        this.logService = logService;
    }

    /**
     * 含有@Log注解的方法
     */
    @Pointcut("@annotation(com.abing.permissionsys.log.aop.Log)")
    public void logPointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }
    /**
     * Controller包下所有类的所有方法
     */
    @Pointcut("execution(* com.abing.permissionsys.controller.*Controller.*(..))")
    public void controllerCell() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    /**
     *排除
     */
        @Pointcut("execution(* com.abing.permissionsys.controller.*Controller.initEnumList(..))||execution(* com.abing.permissionsys.controller.*Controller.getVerify(..))||execution(* com.abing.permissionsys.controller.LogController.*(..))")
    public void exclude() {
    }

    /**
     * 配置环绕通知,使用在方法logPointcut()上注册的切入点
     *
     * @param joinPoint join point for advice
     */
//    @Around("logPointcut()")
    @Around(value = "(logPointcut()||controllerCell())&&!exclude()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        currentTime = System.currentTimeMillis();
        result = joinPoint.proceed();
        SysLogVO sysLogVO = new SysLogVO(SysLogEnums.LogTypeEnum.LogTypeEnum_0.getIndex(),System.currentTimeMillis() - currentTime);
        HttpServletRequest request = RequestHolder.getHttpServletRequest();
        logService.save(getUsername(), StringUtils.getBrowser(request), StringUtils.getIp(request),joinPoint, sysLogVO);
        return result;
    }

    /**
     * 配置异常通知
     *
     * @param joinPoint join point for advice
     * @param e exception
     */
//    @AfterThrowing(pointcut = "logPointcut()", throwing = "e")
    @AfterThrowing(pointcut = "(logPointcut()||controllerCell()&&!exclude())", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        SysLogVO sysLogVO = new SysLogVO(SysLogEnums.LogTypeEnum.LogTypeEnum_1.getIndex(),System.currentTimeMillis() - currentTime);
        sysLogVO.setExceptionDetail(ThrowableUtil.getStackTrace(e));
        HttpServletRequest request = RequestHolder.getHttpServletRequest();
        logService.save(getUsername(), StringUtils.getBrowser(request), StringUtils.getIp(request), (ProceedingJoinPoint)joinPoint, sysLogVO);
    }

    public String getUsername() {
        try {
            return SecurityUtils.getUsername();
        }catch (Exception e){
            return "";
        }
    }
}
