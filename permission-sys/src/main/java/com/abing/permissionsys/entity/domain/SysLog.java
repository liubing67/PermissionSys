package com.abing.permissionsys.entity.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;


/**
 * 系统日志
 *
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysLog implements Serializable {
	private static final long serialVersionUID = 1L;

	//
	@TableId(value = "id",type = IdType.AUTO)
	private Long id;
	//用户id
	private Long userId;
	//用户名
	private String username;
	//用户操作
	private String operation;
	//响应时间
	private Long time;
	//请求方法
	private String method;
	//请求参数
	private String params;
	//IP地址
	private String ip;
	//创建时间
	private Date gmtCreate;
	//错误详细
	private String exceptionDetail;
	//日志类型（错误日志，操作日志）
	private String logType;
	//地址来源
	private String address;
	//浏览器类型
	private String browser;

}
