package com.abing.permissionsys.controller;

import com.abing.base.BaseApiService;
import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.vo.sysinfo.SystemHardwareInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/sysInfo")
@RestController
@Slf4j
public class SysInfoController extends BaseApiService {



    @RequestMapping("/getSysInfo")
    @ResponseBody()
    BaseResponse getSysInfo() {
        SystemHardwareInfo systemHardwareInfo = new SystemHardwareInfo();
        systemHardwareInfo.copyTo();
        return setResultSuccess(systemHardwareInfo);
    }
}
