package com.abing.permissionsys.service;


import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleMenuService {

    /**
     * 通过角色查询角色对应的菜单
     * @param roleId
     * @return
     */
    List<Long> getMenuIdsByRoleId(Long roleId);

    /**
     * 保存角色对应的菜单
     * @param roleId
     * @return
     */
    void saveMenuByRoleId(String roleId,List<String> menuIds);
}
