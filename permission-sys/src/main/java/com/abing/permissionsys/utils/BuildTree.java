package com.abing.permissionsys.utils;

import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.domain.Tree;
import com.abing.permissionsys.entity.vo.SysDeptVO;
import com.abing.permissionsys.entity.vo.SysMenuVO;
import com.abing.permissionsys.entity.vo.dept.DeptVO;
import com.abing.permissionsys.entity.vo.menu.MenuMetaVo;
import com.abing.permissionsys.entity.vo.menu.MenuVo;

import java.util.ArrayList;
import java.util.List;

public class BuildTree {

    public static <T> Tree<T> build(List<Tree<T>> nodes) {

        if (nodes == null) {
            return null;
        }
        List<Tree<T>> topNodes = new ArrayList<Tree<T>>();

        for (Tree<T> children : nodes) {

            String pid = children.getParentId();
            if (pid == null || "0".equals(pid)) {
                topNodes.add(children);

                continue;
            }

            for (Tree<T> parent : nodes) {
                String id = parent.getId();
                if (id != null && id.equals(pid)) {
                    parent.getChildren().add(children);
                    children.setHasParent(true);
                    parent.setHasChildren(true);
                    continue;
                }
            }

        }

        Tree<T> root = new Tree<T>();
        if (topNodes.size() == 1) {
            root = topNodes.get(0);
        } else {
            root.setId("-1");
            root.setParentId("");
            root.setHasParent(false);
            root.setHasChildren(true);
            root.setChecked(true);
            root.setChildren(topNodes);
            root.setName("顶级节点");
            root.setOpen(true);
        }

        return root;
    }

    public static <T> List<Tree<T>> buildList(List<Tree<T>> nodes, String idParam) {
        if (nodes == null) {
            return null;
        }
        List<Tree<T>> topNodes = new ArrayList<Tree<T>>();

        for (Tree<T> children : nodes) {

            String pid = children.getParentId();
            if (pid == null || idParam.equals(pid)) {
                topNodes.add(children);

                continue;
            }

            for (Tree<T> parent : nodes) {
                String id = parent.getId();
                if (id != null && id.equals(pid)) {
                    parent.getChildren().add(children);
                    children.setHasParent(true);
                    parent.setHasChildren(true);

                    continue;
                }
            }

        }
        return topNodes;
    }

    public static List<MenuVo> buildLists(List<SysMenuVO> nodes) {
        if (nodes == null) {
            return null;
        }
        List<MenuVo> topNodes = new ArrayList<MenuVo>();

        for (SysMenuVO sysMenuVO : nodes) {
            MenuVo menuVo = new MenuVo();
            Long mid = sysMenuVO.getId();
            Long pid = sysMenuVO.getParentId();
            if (0 != pid) {
                continue;
            }
            if (pid == null || 0 == pid) {
                if (StringUtils.isNotEmpty(sysMenuVO.getUrl())&&sysMenuVO.getUrl().startsWith("http")) {
                    menuVo.setPath(StringUtils.isNotEmpty(sysMenuVO.getUrl()) ? sysMenuVO.getUrl() : "/");
                    menuVo.setComponent("Layout");
                    List<MenuVo> cMenuVoS = new ArrayList<MenuVo>();
                    MenuVo cMenuVo = new MenuVo();
                    cMenuVo.setPath(sysMenuVO.getUrl());
                    cMenuVo.setMeta(new MenuMetaVo(sysMenuVO.getName(), sysMenuVO.getIcon()));
                    cMenuVoS.add(cMenuVo);
                    menuVo.setChildren(cMenuVoS);
                } else {
                    menuVo.setAlwaysShow(true);
                    menuVo.setRedirect("noredirect");
                    menuVo.setName(sysMenuVO.getName());
                    menuVo.setPath(StringUtils.isNotEmpty(sysMenuVO.getUrl()) ? sysMenuVO.getUrl() : "/Layout"+mid);
                    menuVo.setComponent(StringUtils.isNotEmpty(sysMenuVO.getUrl()) ? sysMenuVO.getUrl() : "Layout");
//					menuVo.setPath(StringUtils.isNotEmpty(sysMenuVO.getUrl())?sysMenuVO.getUrl():"/");
//					menuVo.setComponent("Layout");
                    menuVo.setMeta(new MenuMetaVo(sysMenuVO.getName(), sysMenuVO.getIcon()));
                    menuVo.setChildren(buildChildrenMenu(nodes, mid));
                }
            }
            topNodes.add(menuVo);
        }
        return topNodes;
    }

    private static List<MenuVo> buildChildrenMenu(List<SysMenuVO> menuVOList, Long menuId) {
        List<MenuVo> childrenMenuVoS = new ArrayList<MenuVo>();
        for (SysMenuVO children : menuVOList) {
            Long mid = children.getId();
            Long id = children.getParentId();
            if (id != null && id == menuId) {
                MenuVo childrenMenuVo = new MenuVo();
                 if (StringUtils.isNotEmpty(children.getUrl())&&children.getUrl().startsWith("http")) {
                    childrenMenuVo.setPath(children.getUrl());
                    childrenMenuVo.setComponent("Layout");
                    List<MenuVo> cMenuVoS = new ArrayList<MenuVo>();
                    MenuVo cMenuVo = new MenuVo();
                    cMenuVo.setPath(children.getUrl());
                    cMenuVo.setMeta(new MenuMetaVo(children.getName(), children.getIcon()));
                    cMenuVoS.add(cMenuVo);
                    childrenMenuVo.setChildren(cMenuVoS);
                } else {
                    childrenMenuVo.setName(children.getName());
                    childrenMenuVo.setPath(StringUtils.isNotEmpty(children.getUrl()) ? children.getUrl() : "/Layout"+mid);
                    childrenMenuVo.setComponent(StringUtils.isNotEmpty(children.getUrl()) ? children.getUrl() : "Layout");
//			childrenMenuVo.setComponent(StringUtils.isNotEmpty(children.getUrl())?"system/"+children.getUrl():"Layout");
                    childrenMenuVo.setMeta(new MenuMetaVo(children.getName(), children.getIcon()));
                    childrenMenuVo.setChildren(buildChildrenMenu(menuVOList, mid));
                }
                childrenMenuVoS.add(childrenMenuVo);
            }

        }
        return childrenMenuVoS;
    }


    private static String getName(String url){
        if (StringUtils.isNotEmpty(url)){
            String[] strs=url.split("/");
            System.out.print("------"+strs.toString());
            for(int i=0,len=strs.length;i<len;i++){
                System.out.print(strs[i].toString()+" ");
            }
//            String str = strs[strs.length-1];
//            return str;
        }
        return "";

    }
    public static DeptVO buildDept(List<SysDeptVO> sysDeptVOS) {

        if (sysDeptVOS == null) {
            return null;
        }

        DeptVO deptVO=new DeptVO();
        List<DeptVO> deptVOS=new ArrayList<DeptVO>();
        for (SysDeptVO sysDeptVO : sysDeptVOS) {

            Long pid = sysDeptVO.getParentId();
            Long id = sysDeptVO.getId();
            DeptVO pDeptVO=new DeptVO();
            if (pid == null || 0 ==pid) {
                pDeptVO= BeanUtil.doToVo(sysDeptVO,DeptVO.class);
                pDeptVO.setChildren(buildChildrenDept(sysDeptVOS,id));
                deptVOS.add(pDeptVO);
            }

        }

        if (deptVOS.size() == 1) {
            deptVO = deptVOS.get(0);
        } else {
            deptVO.setId(0l);
            deptVO.setParentId(0l);
            deptVO.setChildren(deptVOS);
            deptVO.setName("顶级节点");
        }

        return deptVO;
    }
    private static List<DeptVO> buildChildrenDept(List<SysDeptVO> deptVOList, Long deptId) {
        List<DeptVO> deptChildrenVOS=new ArrayList<DeptVO>();
        for (SysDeptVO parent : deptVOList) {
            Long id = parent.getId();
            Long pid = parent.getParentId();
            if (id != null && pid==deptId) {
                DeptVO deptChildrenVo=BeanUtil.doToVo(parent,DeptVO.class);
                deptChildrenVo.setChildren(buildChildrenDept(deptVOList,id));
                deptChildrenVOS.add(deptChildrenVo);
            }
        }
        return deptChildrenVOS;
    }
}
