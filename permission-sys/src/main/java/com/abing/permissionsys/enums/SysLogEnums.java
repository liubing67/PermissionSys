package com.abing.permissionsys.enums;

public class SysLogEnums {

    public enum LogTypeEnum{

        LogTypeEnum_0("0","操作日志"),
        LogTypeEnum_1("1","错误日志");
        private String index;
        private String name;


        LogTypeEnum(String index,String name){
            this.index=index;
            this.name=name;
        }
        public static String getName(String index) {
            for (LogTypeEnum code : LogTypeEnum.values()) {
                if (code.getIndex().equals(index)) {
                    return code.name;
                }
            }
            return null;
        }

        public static String getIndex(String name) {
            for (LogTypeEnum code : LogTypeEnum.values()) {
                if (code.getName().equals(name)) {
                    return code.index;
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }
    }
}
