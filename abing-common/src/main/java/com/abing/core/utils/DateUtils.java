package com.abing.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * @Date: Created in 2018年7月12日 上午11:00:53
 * @Since: JDK1.8
 * @Version: 1.0
 */
public class DateUtils {
	// aa
	public static final String PATTERN_DATE_YEAR = "yyyy";
	public static final String PATTERN_DATE_MONTH = "yyyyMM";
	public static final String PATTERN_DATE_DAY = "yyyyMMdd";
	public static final String PATTERN_DATE_DAY2 = "yyMMdd";

	public static final String PATTERN_DATE_SHORT = "yyyyMMdd";
	public static final String PATTERN_DATE = "yyyy-MM-dd";
	public static final String PATTERN_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String PATTERN_DATE_TIME_NOSECOND = "yyyy-MM-dd HH:mm";
	public static final String PATTERN_DATE_MINUTES = "yyyy-MM-dd HH:mm";
	public static final String PATTERN_TIME = "HH:mm:ss";
	public static final String PATTERN_TIME_INT = "HHmmss";

	//
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss SSS";
	public static final String DEFAULT_DATE_FORMAT2 = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_DATE_FORMAT3 = "yyyy/MM/dd HH:mm:ss";
	public static final String HH_MM_FORMAT = "HH:mm";
	public static final String DATETIME_FORMAT = "yyyyMMddHHmmssSSS";
	public static final String YYYYMMDD_FORMAT = "yyyyMMdd";
	public static final String YYYYMMDDHHMM_FORMAT = "yyyyMMddHHmm";
	public static final String YYYYMMDDHHMMSS_FORMAT = "yyyyMMddHHmmss";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String MM_DD_FORMAT = "MM月dd日";

	//
	public static final String DATE_TYPE_DAY = "DAY";
	public static final String DATE_TYPE_MONTH = "MONTH";
	public static final String DATE_TYPE_YEAR = "YEAR";
	public static final String DATE_TYPE_WEEK = "WEEK";
	public static final String DATE_TYPE_HOUR = "HOUR";
	public static final String DATE_TYPE_MINUTE = "MINUTE";
	public static final String DATE_TYPE_SECOND = "SECOND";

	public static final long MSEC_OF_MIN = 60000;

	/**
	 * @Author: TianYiWu
	 * @Description:时间格式——时间转字符串
	 * @param d
	 *            日期
	 * @param formatPattern
	 *            格式
	 * @return String 日期字符串
	 * @Date: 2018年5月29日 下午3:48:53
	 */
	public static String formatDateToString(Date d, String formatPattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatPattern);

		return dateFormat.format(d);
	}

	/**
	 * @Author: TianYiWu
	 * @Description:时间格式——字符串转时间
	 * @param dateString
	 *            日期字符串
	 * @param formatPattern
	 *            格式
	 * @return 日期
	 * @throws ParseException
	 *             Date
	 * @Date: 2018年5月29日 下午3:49:40
	 */
	public static Date formatStringToDate(String dateString, String formatPattern) throws ParseException {

		return StringUtils.isBlank(dateString) ? null : new SimpleDateFormat(formatPattern).parse(dateString);
	}

	/**
	 * @Author: TianYiWu
	 * @Description: 时间格式——字符串转字符串
	 * @param ori
	 *            输入日期
	 * @param oriFormat
	 *            输入日期类型
	 * @param stringFormat
	 *            目标日期类型
	 * @return 返回目标格式日期字符串
	 * @throws ParseException
	 *             String
	 * @Date: 2018年5月29日 下午3:50:12
	 */
	public static String FormatStringToString(String ori, String oriFormat, String stringFormat) throws ParseException {
		if (ori == null) {
			return "";
		} else {
			if (oriFormat == null || "".equals(oriFormat)) {
				oriFormat = YYYYMMDDHHMMSS_FORMAT;
			}

			if (stringFormat == null || "".equals(stringFormat)) {
				stringFormat = DEFAULT_DATE_FORMAT;
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat(oriFormat);
			Date date = dateFormat.parse(ori);
			SimpleDateFormat format = new SimpleDateFormat(stringFormat);
			return format.format(date);
		}

	}

	public static Date adjustDate(Date d, int calendarType, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);

		cal.add(calendarType, amount);

		return cal.getTime();
	}

	public static Date ignoreTime(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getCutMinuteDate(Date d, int cutInterval) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);

		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		int minute = cal.get(Calendar.MINUTE);
		cal.set(Calendar.MINUTE, (minute / cutInterval) * cutInterval);

		return cal.getTime();
	}

	public static List<Date> getYearList(Date from, Date to) {
		Calendar fromCal = Calendar.getInstance();
		Calendar toCal = Calendar.getInstance();

		fromCal.setTime(from);
		fromCal.set(Calendar.MONTH, 0);
		fromCal.set(Calendar.DAY_OF_MONTH, 1);
		fromCal.set(Calendar.HOUR_OF_DAY, 0);
		fromCal.set(Calendar.MINUTE, 0);
		fromCal.set(Calendar.SECOND, 0);
		fromCal.set(Calendar.MILLISECOND, 0);

		toCal.setTime(to);
		toCal.set(Calendar.MONTH, 0);
		toCal.set(Calendar.DAY_OF_MONTH, 1);
		toCal.set(Calendar.HOUR_OF_DAY, 0);
		toCal.set(Calendar.MINUTE, 0);
		toCal.set(Calendar.SECOND, 0);
		toCal.set(Calendar.MILLISECOND, 0);

		List<Date> returnList = new ArrayList<Date>();
		while (!toCal.before(fromCal)) {
			returnList.add(fromCal.getTime());
			fromCal.add(Calendar.MONTH, 1);
		}

		return returnList;
	}

	public static String getQuarterName(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		int month = cal.get(Calendar.MONTH);
		String quarter = "A";

		if (month < 3) {
			quarter = "A";
		} else if (month < 6) {
			quarter = "B";
		} else if (month < 9) {
			quarter = "C";
		} else {
			quarter = "D";
		}

		return quarter;
	}

	public static List<Date> getQuarterList(Date from, Date to) {
		Calendar fromCal = Calendar.getInstance();
		Calendar toCal = Calendar.getInstance();

		fromCal.setTime(from);
		fromCal.set(Calendar.DAY_OF_MONTH, 1);
		fromCal.set(Calendar.HOUR_OF_DAY, 0);
		fromCal.set(Calendar.MINUTE, 0);
		fromCal.set(Calendar.SECOND, 0);
		fromCal.set(Calendar.MILLISECOND, 0);

		toCal.setTime(to);
		toCal.set(Calendar.DAY_OF_MONTH, 1);
		toCal.set(Calendar.HOUR_OF_DAY, 0);
		toCal.set(Calendar.MINUTE, 0);
		toCal.set(Calendar.SECOND, 0);
		toCal.set(Calendar.MILLISECOND, 0);

		List<Date> returnList = new ArrayList<Date>();
		while (!toCal.before(fromCal)) {
			returnList.add(fromCal.getTime());
			fromCal.add(Calendar.MONTH, 3);
			int month = fromCal.get(Calendar.MONTH);
			int firstMonthOfFromCal;

			if (month < 3) {
				firstMonthOfFromCal = 0;
			} else if (month < 6) {
				firstMonthOfFromCal = 3;
			} else if (month < 9) {
				firstMonthOfFromCal = 6;
			} else {
				firstMonthOfFromCal = 9;
			}
			fromCal.set(Calendar.MONTH, firstMonthOfFromCal);
		}

		return returnList;
	}

	public static int getWeekOfDate(Date d) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(d);

		return returnCal.get(Calendar.WEEK_OF_YEAR);
	}

	public static List<Date> getMonthList(Date from, Date to) {
		Calendar fromCal = Calendar.getInstance();
		Calendar toCal = Calendar.getInstance();

		fromCal.setTime(from);
		fromCal.set(Calendar.DAY_OF_MONTH, 1);
		fromCal.set(Calendar.HOUR_OF_DAY, 0);
		fromCal.set(Calendar.MINUTE, 0);
		fromCal.set(Calendar.SECOND, 0);
		fromCal.set(Calendar.MILLISECOND, 0);

		toCal.setTime(to);
		toCal.set(Calendar.DAY_OF_MONTH, 1);
		toCal.set(Calendar.HOUR_OF_DAY, 0);
		toCal.set(Calendar.MINUTE, 0);
		toCal.set(Calendar.SECOND, 0);
		toCal.set(Calendar.MILLISECOND, 0);

		List<Date> returnList = new ArrayList<Date>();
		while (!toCal.before(fromCal)) {
			returnList.add(fromCal.getTime());
			fromCal.add(Calendar.MONTH, 1);
		}

		return returnList;
	}

	public static List<Date> getDayList(Date from, Date to) {
		Calendar fromCal = Calendar.getInstance();
		Calendar toCal = Calendar.getInstance();

		fromCal.setTime(from);
		fromCal.set(Calendar.HOUR_OF_DAY, 0);
		fromCal.set(Calendar.MINUTE, 0);
		fromCal.set(Calendar.SECOND, 0);
		fromCal.set(Calendar.MILLISECOND, 0);

		toCal.setTime(to);
		toCal.set(Calendar.HOUR_OF_DAY, 0);
		toCal.set(Calendar.MINUTE, 0);
		toCal.set(Calendar.SECOND, 0);
		toCal.set(Calendar.MILLISECOND, 0);

		List<Date> returnList = new ArrayList<Date>();
		while (!toCal.before(fromCal)) {
			returnList.add(fromCal.getTime());
			fromCal.add(Calendar.DAY_OF_YEAR, 1);
		}

		return returnList;
	}

	//
	public static Date convertToSameDayTime(final Date destDate, final Date dameDate) {
		Calendar returnCal = Calendar.getInstance();

		Calendar today = Calendar.getInstance();
		today.setTime(dameDate);

		returnCal.setTime(destDate);

		returnCal.set(Calendar.YEAR, today.get(Calendar.YEAR));
		returnCal.set(Calendar.MONTH, today.get(Calendar.MONTH));
		returnCal.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH));

		return returnCal.getTime();
	}

	public static Date getYesterday(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);
		returnCal.add(Calendar.DATE, -1);

		return returnCal.getTime();
	}

	public static Date getLastWeek(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);
		returnCal.add(Calendar.WEEK_OF_MONTH, -1);

		return returnCal.getTime();
	}

	public static Date getLastSunday(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);
		returnCal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		return returnCal.getTime();
	}

	public static Date getThisWeek(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);
		returnCal.add(Calendar.WEEK_OF_MONTH, 0);

		return returnCal.getTime();
	}

	public static Date getLastMonth(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);
		returnCal.set(Calendar.DATE, 1);// 设为当前月的1号
		returnCal.add(Calendar.MONTH, -1);// 减一个月，变为下月的1号

		return returnCal.getTime();
	}

	public static Date getFirstDayOfMonth(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);

		returnCal.set(Calendar.DATE, 1);// 设为当前月的1号
		return returnCal.getTime();
	}

	public static Date getLastDayOfMonth(final Date nowDate) {
		Calendar returnCal = Calendar.getInstance();
		returnCal.setTime(nowDate);

		returnCal.add(Calendar.MONTH, 1);// 变为下月的1号
		returnCal.set(Calendar.DATE, 1);// 设为下月的1号
		returnCal.add(Calendar.DATE, -1);
		return returnCal.getTime();
	}

	// check the day if the first day of the month.
	public static boolean isFirstDayOfMonth(Date d) {
		Calendar day = Calendar.getInstance();
		day.setTime(d);

		return day.get(Calendar.DAY_OF_MONTH) == 1;
	}

	// check the day if the first day of the year.
	public static boolean isFirstDayOfYear(Date d) {
		Calendar day = Calendar.getInstance();
		day.setTime(d);

		return day.get(Calendar.DAY_OF_MONTH) == 1 && day.get(Calendar.MONTH) == 1;
	}

	public static long ignoreSec(long t) {
		long min = t / MSEC_OF_MIN;
		long left = t % MSEC_OF_MIN;

		if ((left * 2) >= MSEC_OF_MIN) {
			min++;
		}

		return min * MSEC_OF_MIN;
	}

	/**
	 * 将Date转换为字符串
	 *
	 * @param date
	 *            Date 要转换的日期
	 * @param dateFormatStr
	 *            String 要转换的日期类型
	 * @return String 返加String类型的日期
	 */
	public static String DateToString(Date date, String dateFormatStr) {
		if (date == null) {
			return "";
		} else {
			if (dateFormatStr == null || "".equals(dateFormatStr)) {
				dateFormatStr = DEFAULT_DATE_FORMAT;
			}
			SimpleDateFormat simpleDteFormat = new SimpleDateFormat(dateFormatStr);
			return simpleDteFormat.format(date);
		}

	}

	/**
	 * 返回当前日期时间的字符串
	 *
	 * @param dateFormatStr
	 *            String 要转换的日期类型
	 * @return String 返加String类型的日期
	 */
	public static String getCurrentDateTime(String dateFormatStr) {
		if (dateFormatStr == null || "".equals(dateFormatStr)) {
			dateFormatStr = DEFAULT_DATE_FORMAT2;
		}
		Date date = new Date();
		SimpleDateFormat simpleDteFormat = new SimpleDateFormat(dateFormatStr);
		return simpleDteFormat.format(date);
	}

	/**
	 * 返回当前日期的字符串
	 *
	 * @param dateFormatStr
	 *            String 日期格式
	 * @return String 返回当前日期的字符串
	 */
	public static String getCurrentDate(String dateFormatStr) {
		if (dateFormatStr == null || "".equals(dateFormatStr)) {
			dateFormatStr = DATE_FORMAT;
		}
		Date date = new Date();
		SimpleDateFormat simpleDteFormat = new SimpleDateFormat(dateFormatStr);
		return simpleDteFormat.format(date);
	}

	/**
	 * 将字符串转换为Date
	 *
	 * @param strDate
	 *            String 被转换的String类型的日期
	 * @param strDateFormat
	 *            String Date格式
	 * @return Date 返加Date类型的日期
	 */
	public static Date StringTodate(String strDate, String strDateFormat) {
		if (StringUtils.isEmpty(strDate)) {
			return null;
		} else {
			if ("".equals(strDateFormat) || strDateFormat == null) {
				strDateFormat = DEFAULT_DATE_FORMAT2;
			}
			Date rDate;
			SimpleDateFormat format = new SimpleDateFormat(strDateFormat);
			try {
				rDate = format.parse(strDate);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			return rDate;
		}
	}

	/**
	 * 将string转换成指定类型的Timestamp
	 *
	 * @param str
	 *            String 被转换的String类型的日期
	 * @param type
	 *            String 日期格式
	 * @return Timestamp 返加Timestamp类型的日期
	 */
	public static Timestamp cString2Timestamp(String str, String type) {
		if (type == null || type.equals("")) {
			type = DEFAULT_DATE_FORMAT2;
		}
		if (str.length() <= 10) {
			str = str + " 00:00:00";
		}
		SimpleDateFormat df = new SimpleDateFormat(type);
		try {
			return new Timestamp(df.parse(str).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Timestamp getTimeStamp() {
		return cString2Timestamp(getCurrentDateTime(null), null);
	}

	/**
	 * 将Timestamp转换成指定类型的string
	 *
	 * @param ts
	 *            Timestamp 被转换的Timestamp类型的日期
	 * @param type
	 *            String 日期格式
	 * @return String 返加String类型的日期
	 */
	public static String cTimestamp2String(Timestamp ts, String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		return df.format(new Date(ts.getTime()));
	}

	/**
	 * 将Timestamp转换成date
	 *
	 * @param ts
	 *            Timestamp 被转换的Timestamp类型的日期
	 * @return Date 返加Date类型的日期
	 */
	public static Date cTimestamp2Date(Timestamp ts) {
		String strDate = cTimestamp2String(ts, DEFAULT_DATE_FORMAT2);
		return StringTodate(strDate, DEFAULT_DATE_FORMAT2);
	}

	/**
	 * 日期相加函数
	 *
	 * @param sorDate
	 *            String 被加的日期,必须为String类型YYYYMMDD
	 * @param value
	 *            int 天数,可为负数
	 * @param dateType
	 *            String 日期格式
	 * @return String 相加后的日期,必须为String类型
	 */
	public static String dateAdd(String sorDate, int value, String dateType) {
		if (dateType == null || value == 0 || sorDate == null) {
			return sorDate;
		}
		Date date = DateUtils.StringTodate(sorDate, YYYYMMDD_FORMAT);
		Date getDate = DateUtils.dateAdd(date, value, dateType);
		return DateUtils.DateToString(getDate, YYYYMMDD_FORMAT);
	}

	/**
	 * 日期相加函数
	 *
	 * @param sorDate
	 *            Date 被加的日期,必须为Date类型
	 * @param value
	 *            int 天数,可为负数
	 * @param dateType
	 *            String 日期格式
	 * @return Date 相加后的日期
	 */
	public static Date dateAdd(Date sorDate, int value, String dateType) {
		if (dateType == null || value == 0 || sorDate == null) {
			return sorDate;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sorDate);
		if (dateType.equalsIgnoreCase(DATE_TYPE_SECOND)) {
			calendar.add(Calendar.SECOND, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_MINUTE)) {
			calendar.add(Calendar.MINUTE, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_HOUR)) {
			calendar.add(Calendar.HOUR, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_WEEK)) {
			calendar.add(Calendar.WEDNESDAY, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_DAY)) {
			calendar.add(Calendar.DATE, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_MONTH)) {
			calendar.add(Calendar.MONTH, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_YEAR)) {
			calendar.add(Calendar.YEAR, value);
		}
		return calendar.getTime();
	}

	/**
	 * 日期相加函数
	 *
	 * @param sorDate
	 *            Date 被加的日期,必须为Date类型
	 * @param dateFormat
	 *            String 日期格式
	 * @param value
	 *            int 数量,可为负数
	 * @param dateType
	 *            String 日期类型(年、月、星期、日、时、分、秒)
	 * @return Date 相加后的日期
	 */
	public static String dateAdd(String sorDate, String dateFormat, int value, String dateType) {
		if (dateType == null || value == 0 || sorDate == null) {
			return sorDate;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(StringTodate(sorDate, dateFormat));
		if (dateType.equalsIgnoreCase(DATE_TYPE_SECOND)) {
			calendar.add(Calendar.SECOND, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_MINUTE)) {
			calendar.add(Calendar.MINUTE, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_HOUR)) {
			calendar.add(Calendar.HOUR, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_WEEK)) {
			calendar.add(Calendar.WEDNESDAY, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_DAY)) {
			calendar.add(Calendar.DATE, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_MONTH)) {
			calendar.add(Calendar.MONTH, value);
		} else if (dateType.equalsIgnoreCase(DATE_TYPE_YEAR)) {
			calendar.add(Calendar.YEAR, value);
		}
		return DateToString(calendar.getTime(), dateFormat);
	}

	// 年份是否相同
	public static boolean isSameYear(Date from, Date to) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(from);
		int fromy = cal.get(Calendar.YEAR);
		cal.setTime(to);
		int toy = cal.get(Calendar.YEAR);
		return fromy == toy;
	}

	/**
	 * 判断日期是否是今天
	 *
	 * @param from
	 * @return
	 */
	public static boolean isToday(Date from) {
		Date to = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(from);
		long fromd = cal.get(Calendar.DAY_OF_YEAR);
		cal.setTime(to);
		long tod = cal.get(Calendar.DAY_OF_YEAR);
		return isSameYear(from, to) && fromd == tod;
	}

	/**
	 * 判断日期是否是昨天
	 *
	 * @param from
	 * @return
	 */
	public static boolean isYesterDay(Date from) {
		Date to = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(from);
		int fromd = cal.get(Calendar.DAY_OF_YEAR);
		cal.setTime(to);
		int tod = cal.get(Calendar.DAY_OF_YEAR);
		return isSameYear(from, to) && ((fromd + 1) == tod);
	}

	/**
	 * 将时间转换成 昨天 12:30 / 今天 12:40 / 10分钟前 //两个小时前
	 *
	 * @param date
	 * @return
	 */
	public static String parseDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date == null) {
			return "";
		}
		calendar.setTime(date);

		if (isToday(date)) {// 是否今天 DEFAULT_HOUR_MINITE_FORMAT
			return "今天 " + DateToString(date, DateUtils.HH_MM_FORMAT);
		}

		if (isYesterDay(date)) {// 是否昨天
			return "昨天 " + DateToString(date, DateUtils.HH_MM_FORMAT);
		}

		if (isSameYear(date, new Date())) {// 是否今年
			return DateToString(date, DateUtils.MM_DD_FORMAT);
		}

		return DateToString(date, DateUtils.DATE_FORMAT);
	}

	public static String cSecond2HHmmss(long seconds) {
		String timeStr = "";

		long hour = seconds / 3600;
		long minute = seconds % 3600 / 60;
		long second = seconds % 60;

		if (hour > 0) {
			if (hour < 10) {
				timeStr = "0" + hour + ":";
			} else {
				timeStr = hour + ":";
			}
		}

		if (minute < 10) {
			timeStr = timeStr + "0" + minute + ":";
		} else {
			timeStr = timeStr + minute + ":";
		}

		if (second < 10) {
			timeStr = timeStr + "0" + second;
		} else {
			timeStr = timeStr + second;
		}

		return timeStr;
	}

	/**
	 * 将时间转换成 几秒前 / 几分钟之前 / 几小时前 // 1天前、3天前//1年前
	 */
	public static String parseDhms(Date from) {
		Date to = new Date();// 当前日期

		int severalDay = Integer.parseInt(DateToString(to, PATTERN_DATE_DAY)) - Integer.parseInt(DateToString(from, PATTERN_DATE_DAY));

		if (severalDay == 0) {
			String dateFrom = DateToString(from, PATTERN_TIME_INT);
			int hhFrom = Integer.parseInt(dateFrom.substring(0, 2));
			int mmFrom = Integer.parseInt(dateFrom.substring(2, 4));
			int ssFrom = Integer.parseInt(dateFrom.substring(4, 6));

			String dateTo = DateToString(to, PATTERN_TIME_INT);
			int hhTo = Integer.parseInt(dateTo.substring(0, 2));
			int mmTo = Integer.parseInt(dateTo.substring(2, 4));
			int ssTo = Integer.parseInt(dateTo.substring(4, 6));

			if (hhTo > hhFrom) {
				return (hhTo - hhFrom) + "小时前";
			} else if (mmTo > mmFrom) {
				return (mmTo - mmFrom) + "分钟前";
			} else {
				System.out.println(dateFrom + ":" + dateTo);
				return (ssTo - ssFrom) + "秒前";
			}

		} else if (severalDay > 0 && severalDay <= 3) {
			return severalDay + "天前";
		} else {

			return parseDate(from);
		}

	}

	// 得到相差天数
	public static long getDiffDay(String startTime, String endTime, String format) {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long diff;
		try {
			// 获得两个时间的毫秒时间差异
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			long day = diff / nd;// 计算差多少天

			return day;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	// 得到相差天数
		public static long getDiffDays(String startTime, String endTime, String format) throws ParseException {
			// 按照传入的格式生成一个simpledateformate对象
			SimpleDateFormat sd = new SimpleDateFormat(format);
			long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
			long diff;
			try {
				// 获得两个时间的毫秒时间差异
				diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
				long day = diff / nd;// 计算差多少天

				return day;
			} catch (ParseException e) {
				throw e;
			}
		}

	public static long getDiffHour(String startTime, String endTime, String format) {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long diff;
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			// long day = diff / nd;// 计算差多少天
			long hour = diff % nd / nh;// 计算差多少小时
			return hour;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	// 返回相差小时数（包括天）
	public static long getAllDiffHour(String startTime, String endTime, String format) {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long diff;
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			long day = diff / nd;// 计算差多少天
			long hour = diff % nd / nh;// 计算差多少小时
			return day * 24 + hour;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static long getDiffMm(String startTime, String endTime, String format) {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long diff;
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			// long day = diff / nd;// 计算差多少天
			// long hour = diff % nd / nh;// 计算差多少小时
			long min = diff % nd % nh / nm;// 计算差多少分钟
			return min;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static long getDiffSs(String startTime, String endTime, String format) {
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long ns = 1000;// 一秒钟的毫秒数
		long diff;
		try {
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			long sec = diff / ns;// 计算差多少秒
			return sec;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	// 获得时间相差秒数 可跨天
	public static long getDiffSsCrossDate(String startTime, String endTime, String format) {
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long ns = 1000;// 一秒钟的毫秒数
		long ds = 24 * 60 * 60 * 1000;// 一天的毫秒数
		long diff;
		try {
			long sec = 0l;
			diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
			if (diff >= 0) {// 如果不跨天，endTime比startTime大
				sec = diff / ns;// 计算差多少秒
			} else {// 跨天 endTime比StartTime小，diff为负数
				sec = (ds + diff) / ns;
			}
			return sec;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description:处理shop库中表存放的时间戳
	 * 
	 * @param phpTimeSeconds
	 * @param dateFormat
	 *            要转换的时间格式：如：yyyy-MM-dd
	 * @return dateFormat格式转换的日期时间字符串
	 */
	public static String handlePHPTimeSeconds(long phpTimeSeconds, String dateFormat) {
		String returnValue = "";
		if (phpTimeSeconds >= 0) {
			long javaTimeMillis = Long.parseLong(phpTimeSeconds + "000");
			returnValue = cTimestamp2String(new Timestamp(javaTimeMillis), dateFormat);
		}
		return returnValue;
	}

	/**
	 * Added by zhousiyuan 2017-4-5 将时间转到对应时区
	 * 
	 * @param sourceDate
	 *            源时间
	 * @param sourceTimeZone
	 *            源时区
	 * @param targetTimeZone
	 *            目标时区
	 * @return 对应时区的时间
	 */
	public static Date transformByTimeZone(Date sourceDate, String sourceTimeZone, String targetTimeZone) {
		TimeZone sourcetz = TimeZone.getTimeZone(sourceTimeZone);
		TimeZone targettz = TimeZone.getTimeZone(targetTimeZone);
		Long targetTime = sourceDate.getTime() - sourcetz.getRawOffset() + targettz.getRawOffset();
		return new Date(targetTime);
	}

	/**
	 * Added by zhousiyuan 2017-4-5 将时间转到对应时区并格式化
	 * 
	 * @param sourceDate
	 *            源时间
	 * @param sourceTimeZone
	 *            源时区
	 * @param targetTimeZone
	 *            目标时区
	 * @param format
	 *            格式
	 * @return 格式化后的时间字符串
	 */
	public static String transformByTimeZone(Date sourceDate, String sourceTimeZone, String targetTimeZone, String format) {
		return new SimpleDateFormat(format).format(transformByTimeZone(sourceDate, sourceTimeZone, targetTimeZone));
	}

	/**
	 * 将字符串转为日期类型
	 * 
	 * @param s
	 * @return
	 */
	public static Date parseDate(String s) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(s);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 将字符串转为日期类型
	 * 
	 * @param s
	 * @return
	 */
	public static Date parseDateTime(String s) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 时间戳转换成，日期
	 * 
	 * @param s
	 * @return
	 */
	public static Date LongToDate(Long time) {
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTimeInMillis(time);
		return rightNow.getTime();
	}

	public static List<String> getDateList(String startDate, String endDate) {
		List<String> returnList = new ArrayList<String>();
		try {
			SimpleDateFormat df = new SimpleDateFormat(PATTERN_DATE);

			Calendar fromCal = Calendar.getInstance();
			Calendar toCal = Calendar.getInstance();

			fromCal.setTime(df.parse(startDate));
			fromCal.set(Calendar.HOUR_OF_DAY, 0);
			fromCal.set(Calendar.MINUTE, 0);
			fromCal.set(Calendar.SECOND, 0);
			fromCal.set(Calendar.MILLISECOND, 0);

			toCal.setTime(df.parse(endDate));
			toCal.set(Calendar.HOUR_OF_DAY, 0);
			toCal.set(Calendar.MINUTE, 0);
			toCal.set(Calendar.SECOND, 0);
			toCal.set(Calendar.MILLISECOND, 0);

			while (!toCal.before(fromCal)) {
				returnList.add(df.format(fromCal.getTime()));
				fromCal.add(Calendar.DAY_OF_YEAR, 1);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return returnList;
	}

	public static boolean isValidDate(String dateTime) {
		Date dataTime = parseDateTime(dateTime);
		return dataTime != null;
	}

	public static String getNowTimestamp() {
		return System.currentTimeMillis() + "";

	}

	public static Date pinjieHours(Date date, int flag) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		// 时分秒（毫秒数）
		long millisecond = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000;
		// 凌晨00:00:00
		cal.setTimeInMillis(cal.getTimeInMillis() - millisecond);

		if (flag == 0) {
			return cal.getTime();
		} else if (flag == 1) {
			// 凌晨23:59:59
			cal.setTimeInMillis(cal.getTimeInMillis() + 23 * 60 * 60 * 1000 + 59 * 60 * 1000 + 59 * 1000);
		}
		return cal.getTime();
	}

	/**
	 * 在日期上增加数个整月
	 */
	public static Date addMonth(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, n);
		return cal.getTime();
	}

	/**
	 * 获得月末日
	 */
	public static Date getLastDayOfMonth(String year, String month, int flag) {

		Calendar cal = Calendar.getInstance();
		// 年
		cal.set(Calendar.YEAR, Integer.parseInt(year));
		// 月，因为Calendar里的月是从0开始，所以要-1
		cal.set(Calendar.MONTH, Integer.parseInt(month) - 1);
		// 日，设为一号
		cal.set(Calendar.DATE, 1);
		// 月份加一，得到下个月的一号
		cal.add(Calendar.MONTH, flag);
		// 下一个月减一为本月最后一天
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	/**
	 * 获得月末日: flag=1:获得上月月末日 flag=0：获得本月月末日
	 */
	public static Date getLastDayOfMonth(Date date, int flag) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 日，设为一号
		cal.set(Calendar.DATE, 1);
		// 月份加一，得到下个月的一号
		cal.add(Calendar.MONTH, flag);
		// 下一个月减一为本月最后一天
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	/**
	 * 获得周末日
	 */
	public static Date getLastDayOfWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	/**
	 * 在日期上增加数天
	 * 
	 * @param date
	 * @param n
	 * @return
	 */
	public static Date addDay(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, n);
		return cal.getTime();
	}

	/**
	 * 得到当前时间
	 * 
	 * @return HHmmss
	 */
	public static String GetCurrentTime() {
		StringBuffer result = new StringBuffer();
		Calendar calendar = Calendar.getInstance();
		int h = calendar.get(Calendar.HOUR_OF_DAY);
		int m = calendar.get(Calendar.MINUTE);
		int s = calendar.get(Calendar.SECOND);
		if (h < 10) {
			result.append("0");
		}
		result.append(h);
		if (m < 10) {
			result.append("0");
		}
		result.append(m);
		if (s < 10) {
			result.append("0");
		}
		result.append(s);
		return result.toString();
	}

	/**
	 * 得到当前日期时间
	 * 
	 * @return yyyyMMddHHmmss
	 */
	public static String getCurrDateTime() {
		Date now = new Date();
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String s = outFormat.format(now);
		return s;
	}

	/**
	 * Formats a Date object to return a date using the global locale.
	 */
	public static String formatDate(Date date) {
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
		return outFormat.format(date);
	}

	/**
	 * Formats a Date object to return a date and time using the global locale.
	 */
	public static String formatDateTime(Date date) {
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return outFormat.format(date);
	}

	public static String formatDate2(Date myDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String strDate = formatter.format(myDate);
		return strDate;
	}

	public static String formatDate3(Date myDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm");
		String strDate = formatter.format(myDate);
		return strDate;
	}

	public static String formatDate4(Date myDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(myDate);
		return strDate;
	}

	public static String formatDate5(Date myDate) {
		String strDate = getYear(myDate) + "-" + getMonth(myDate) + "-" + getDay(myDate);
		return strDate;
	}

	public static String formatDate6(Date myDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String strDate = formatter.format(myDate);
		return strDate;
	}

	public static long Date2Long(int year, int month, int date) {
		Calendar cld = Calendar.getInstance();
		month = month - 1;
		cld.set(year, month, date);
		return cld.getTime().getTime();
	}

	public static long Time2Long(int year, int month, int date, int hour, int minute, int second) {
		Calendar cld = Calendar.getInstance();
		month = month - 1;
		cld.set(year, month, date, hour, minute, second);
		return cld.getTime().getTime();
	}

	public static int getYear(long t) {
		Calendar cld = Calendar.getInstance();
		if (t > 0) {
			cld.setTime(new Date(t));
		}
		return cld.get(Calendar.YEAR);
	}

	public static int getMonth(long t) {
		Calendar cld = Calendar.getInstance();
		if (t > 0) {
			cld.setTime(new Date(t));
		}
		return cld.get(Calendar.MONTH) + 1;
	}

	public static int getDay(long t) {
		Calendar cld = Calendar.getInstance();
		if (t > 0) {
			cld.setTime(new Date(t));
		}
		return cld.get(Calendar.DAY_OF_MONTH);
	}

	public static int getHour(long t) {
		Calendar cld = Calendar.getInstance();
		if (t > 0) {
			cld.setTime(new Date(t));
		}
		return cld.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(long t) {
		Calendar cld = Calendar.getInstance();
		if (t > 0) {
			cld.setTime(new Date(t));
		}
		return cld.get(Calendar.MINUTE);
	}

	public static int getSecond(long t) {
		Calendar cld = Calendar.getInstance();
		if (t > 0) {
			cld.setTime(new Date(t));
		}
		return cld.get(Calendar.SECOND);
	}

	public static int getYear(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.YEAR);
	}

	public static int getMonth(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.MONTH) + 1;
	}

	public static int getDay(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.DAY_OF_MONTH);
	}

	public static int getHour(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.MINUTE);
	}

	public static int getSecond(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.SECOND);
	}

	public static int getYear() {
		Calendar cld = Calendar.getInstance();
		cld.setTime(new Date());
		return cld.get(Calendar.YEAR);
	}

	public static int getMonth() {
		Calendar cld = Calendar.getInstance();
		cld.setTime(new Date());
		return cld.get(Calendar.MONTH) + 1;
	}

	public static int getDay() {
		Calendar cld = Calendar.getInstance();
		cld.setTime(new Date());
		return cld.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 把日期转成2006/12/30格式
	 *
	 * @param date
	 * @return
	 */
	public static String formatDate(String date) {
		if (date != null && date.trim().length() >= 8) {
			return date.substring(0, 4) + "/" + date.substring(4, 6) + "/" + date.substring(6, 8);
		}
		return date;
	}

	public static String formatTime(String time) {
		if (time != null && time.trim().length() >= 6) {
			return time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, 6);
		}
		return time;
	}

	public static String formateTimeOfDay(String time) {
		if (time != null && time.trim().length() >= 12) {
			return time.substring(0, 4) + "/" + time.substring(4, 6) + "/" + time.substring(6, 8) + " " + time.substring(8, 10) + "：" + time.substring(10, 12);
		}
		return time;
	}

	/**
	 * 获得两天之间相差天数 date-date1
	 *
	 * @param date
	 * @param date1
	 * @return
	 */
	public static int getDiffDate(Date date, Date date1) {
		return (int) ((date.getTime() - date1.getTime()) / (24 * 3600 * 1000));
	}

	/**
	 * 获取两个时间的秒差 date-date1
	 *
	 * @param date
	 * @param date1
	 * @return
	 */
	public static long getDiffTime(Date date, Date date1) {
		return (date.getTime() - date1.getTime()) / 1000;
	}

	/**
	 * 从 yyyyMMddHHmmss格式的时间中 获取日期 yyyyMMdd
	 *
	 * @param time
	 * @return yyyyMMdd
	 */
	public static String getDateFromyyyyMMddHHmmss(String time) {
		return time.substring(0, 8);
	}

	/**
	 * 在日期上增加数个整年
	 */
	public static Date addYear(Date date, int n) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, n);
		return cal.getTime();
	}
	// 获取前天的开始时间
	public static Date getDayBeforeYesterdayBegin() {
		Calendar cal = new GregorianCalendar();
		cal.setTime(getTodayBegin());
		cal.add(Calendar.DAY_OF_MONTH, -2);
		return cal.getTime();
	}
	// 获取昨天的开始时间
	public static Date getYesterdayBegin() {
		Calendar cal = new GregorianCalendar();
		cal.setTime(getTodayBegin());
		cal.add(Calendar.DAY_OF_MONTH, -1);
		return cal.getTime();
	}

	// 获取昨天的结束时间
	public static Date getYesterDayEnd() {
		Calendar cal = new GregorianCalendar();
		cal.setTime(getTodayEnd());
		cal.add(Calendar.DAY_OF_MONTH, -1);
		return cal.getTime();
	}

	// 获取当天的开始时间
	public static Date getTodayBegin() {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	// 获取当天的结束时间
	public static Date getTodayEnd() {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}
	// 获取明天的开始时间
	public static Date getTomorrowBegin() {
		Calendar cal = new GregorianCalendar();
		cal.setTime(getTodayBegin());
		cal.add(Calendar.DAY_OF_MONTH, 1);

		return cal.getTime();
	}

	// 获取明天的结束时间
	public static Date getTomorrowEnd() {
		Calendar cal = new GregorianCalendar();
		cal.setTime(getTodayEnd());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}




	// 获取本周的开始时间
	public static Date getBeginDayOfWeek() {
		Date date = new Date();
		if (date == null) {
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
		if (dayofweek == 1) {
			dayofweek += 7;
		}
		cal.add(Calendar.DATE, 2 - dayofweek);
		return getDayStartTime(cal.getTime());
	}

	// 获取本周的结束时间
	public static Date getEndDayOfWeek() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getBeginDayOfWeek());
		cal.add(Calendar.DAY_OF_WEEK, 6);
		Date weekEndSta = cal.getTime();
		return getDayEndTime(weekEndSta);
	}

	// 获取本月的开始时间
	public static Date getBeginDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(getNowYear(), getNowMonth() - 1, 1);
		return getDayStartTime(calendar.getTime());
	}

	// 获取本月的结束时间
	public static Date getEndDayOfMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(getNowYear(), getNowMonth() - 1, 1);
		int day = calendar.getActualMaximum(5);
		calendar.set(getNowYear(), getNowMonth() - 1, day);
		return getDayEndTime(calendar.getTime());
	}

	// 获取本年的开始时间
	public static Date getBeginDayOfYear() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, getNowYear());
		// cal.set
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DATE, 1);

		return getDayStartTime(cal.getTime());
	}

	// 获取本年的结束时间
	public static Date getEndDayOfYear() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, getNowYear());
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DATE, 31);
		return getDayEndTime(cal.getTime());
	}

	// 获取某个日期的开始时间
	public static Timestamp getDayStartTime(Date d) {
		Calendar calendar = Calendar.getInstance();
		if (null != d)
			calendar.setTime(d);
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}

	// 获取某个日期的结束时间
	public static Timestamp getDayEndTime(Date d) {
		Calendar calendar = Calendar.getInstance();
		if (null != d)
			calendar.setTime(d);
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Timestamp(calendar.getTimeInMillis());
	}

	// 获取今年是哪一年
	public static Integer getNowYear() {
		Date date = new Date();
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return Integer.valueOf(gc.get(1));
	}

	// 获取本月是哪一月
	public static int getNowMonth() {
		Date date = new Date();
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return gc.get(2) + 1;
	}

	// 两个日期相减得到的天数
	public static int getDiffDays(Date beginDate, Date endDate) {

		if (beginDate == null || endDate == null) {
			throw new IllegalArgumentException("getDiffDays param is null!");
		}

		long diff = (endDate.getTime() - beginDate.getTime()) / (1000 * 60 * 60 * 24);

		int days = new Long(diff).intValue();

		return days;
	}

	// 两个日期相减得到的毫秒数
	public static long dateDiff(Date beginDate, Date endDate) {
		long date1ms = beginDate.getTime();
		long date2ms = endDate.getTime();
		return date2ms - date1ms;
	}

	// 获取两个日期中的最大日期
	public static Date max(Date beginDate, Date endDate) {
		if (beginDate == null) {
			return endDate;
		}
		if (endDate == null) {
			return beginDate;
		}
		if (beginDate.after(endDate)) {
			return beginDate;
		}
		return endDate;
	}

	// 获取两个日期中的最小日期
	public static Date min(Date beginDate, Date endDate) {
		if (beginDate == null) {
			return endDate;
		}
		if (endDate == null) {
			return beginDate;
		}
		if (beginDate.after(endDate)) {
			return endDate;
		}
		return beginDate;
	}

	// 返回某月该季度的第一个月
	public static Date getFirstSeasonDate(Date date) {
		final int[] SEASON = { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4 };
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int sean = SEASON[cal.get(Calendar.MONTH)];
		cal.set(Calendar.MONTH, sean * 3 - 3);
		return cal.getTime();
	}

	// 返回某个日期下几天的日期
	public static Date getNextDay(Date date, int i) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) + i);
		return cal.getTime();
	}

	// 返回某个日期前几天的日期
	public static Date getFrontDay(Date date, int i) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) - i);
		return cal.getTime();
	}
	
	/**
	 * @description 获取当日最后一秒(第二天凌晨)
	 * @return
	 */
	public static Date getLastSecond() {
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date zero = calendar.getTime();
        return zero;
	}

	/**
	 * @Title: isInDate
	 * @Description: 判断一个时间段（YYYY-MM-DD）是否在一个区间
	 * @param @param date
	 * @param @param strDateBegin
	 * @param @param strDateEnd
	 * @param @return    设定文件
	 * @return boolean    返回类型
	 * @throws
	 */
	public static boolean isInDate(Date date, String strDateBegin,String strDateEnd) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		String strDate = sdf.format(date);   //2017-04-11
		// 截取当前时间年月日 转成整型
		int  tempDate=Integer.parseInt(strDate.split("-")[0]+strDate.split("-")[1]+strDate.split("-")[2]);
		// 截取开始时间年月日 转成整型
		int  tempDateBegin=Integer.parseInt(strDateBegin.split("-")[0]+strDateBegin.split("-")[1]+strDateBegin.split("-")[2]);
		// 截取结束时间年月日   转成整型
		int  tempDateEnd=Integer.parseInt(strDateEnd.split("-")[0]+strDateEnd.split("-")[1]+strDateEnd.split("-")[2]);

		if ((tempDate >= tempDateBegin && tempDate <= tempDateEnd)) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * 获取当前月第一天
	 * @return
	 */
	public static String getCurrentMonthFirstDay(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	}

	/**
	 * 获取当前月最后一天
	 * @return
	 */
	public static String getCurrentMonthLastDay(){
		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
		return new SimpleDateFormat("yyyy-MM-dd").format(ca.getTime());
	}

	/**
	 * 获取上amount个月第一天
	 * @param @param amount
	 * 上一个月  amount=-1
	 * 上二个月  amount=-2
	 * 上三个月  amount=-3
	 */
	public static String getPrecedingMonthFirstDay(int amount){
		Calendar   cal_1=Calendar.getInstance();//获取当前日期
		cal_1.add(Calendar.MONTH, amount);
		cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
		return new SimpleDateFormat("yyyy-MM-dd").format(cal_1.getTime());
	}
	/**
	 * 获取上amount个月最后一天
	 * @param @param amount
	 * 上一个月  amount=-1
	 * 上二个月  amount=-2
	 * 上三个月  amount=-3
	 */
	public static String getPrecedingMonthLastDay(int amount){
		Calendar cale = Calendar.getInstance();
		cale.add(Calendar.MONTH, amount+1);
		cale.set(Calendar.DAY_OF_MONTH,0);//设置为1号,当前日期既为本月第一天
		return new SimpleDateFormat("yyyy-MM-dd").format(cale.getTime());
	}

	/**
	 * 获取过去第几天的日期(+ 操作) 或者 未来 第几天的日期( - 操作)
	 *
	 * @param past
	 * @return
	 */
	public static String getPastDate(int past,String dateFormatStr) {
		if (dateFormatStr == null || "".equals(dateFormatStr)) {
			dateFormatStr = DEFAULT_DATE_FORMAT;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
		Date today = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat(dateFormatStr);
		String result = format.format(today);
		return result;
	}

	public static LocalDateTime converter(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static Date converter(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
}
