package com.abing.permissionsys.controller;

import com.abing.base.BaseApiService;
import com.abing.base.BaseResponse;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.TimingJobLogVO;
import com.abing.permissionsys.service.TimingJobLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */

@RestController
@Slf4j
@RequestMapping("/timingLog")
public class TimingJobLogController extends BaseApiService {
	@Autowired
	private TimingJobLogService timingLogService;

	@ResponseBody
	@RequestMapping(value = "/listPage")
	public BaseResponse listPage(TimingJobLogVO timingLogVO, PageVal pageVal){
		//查询列表数据
		PageResult sysRole=timingLogService.list(timingLogVO,pageVal);
		log.info("timingJobVO查询：{}",sysRole);
		return setResultSuccess(sysRole);
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	public BaseResponse save( TimingJobLogVO timingLog){
		if(timingLogService.save(timingLog)>0){
			return setResultSuccess();
		}
		return setResultError();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	public BaseResponse update( TimingJobLogVO timingLog){
		timingLogService.update(timingLog);
		return setResultSuccess();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	public BaseResponse remove( Long id){
		if(timingLogService.remove(id)>0){
		return setResultSuccess();
		}
		return setResultError();
	}


}
