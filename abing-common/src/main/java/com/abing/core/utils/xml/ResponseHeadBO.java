package com.abing.core.utils.xml;

import lombok.Data;

@Data
public class ResponseHeadBO {

    private String version;
    private String appid;
    private String function;
    private String respTime;
    private String respTimeZone;
    private String reqMsgId;
}
