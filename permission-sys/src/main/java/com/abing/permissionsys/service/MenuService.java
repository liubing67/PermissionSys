package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.vo.SysMenuVO;
import com.abing.permissionsys.entity.vo.menu.MenuVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MenuService {

	int remove(Long id);

	int save(SysMenuVO menu);

	int update(SysMenuVO menu);

	List<String> listPerms(Long userId);

	/**
	 * 通过appid和userId查询用户和项目对应的菜单
	 * @param userId
	 * @param appId
	 * @return
	 */
	List<MenuVo> getUserMenuByAppId(Long userId, Long appId);

	/**
	 * 通过appId查询应用对应的菜单
	 * @param appId
	 * @return
	 */
	List<SysMenuVO> getMenuByAppId(Long appId);



}
