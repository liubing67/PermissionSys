package com.abing.permissionsys.service.impl;


import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.PageNavigation;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.domain.SysApp;
import com.abing.permissionsys.entity.vo.SysAppVO;
import com.abing.permissionsys.mapper.AppMapper;
import com.abing.permissionsys.service.AppService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mysql.jdbc.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class AppServiceImpl implements AppService {
	@Autowired
	private AppMapper appMapper;

	@Override
	public SysAppVO get(Long id){
		return BeanUtil.doToVo(appMapper.selectById(id),SysAppVO.class);
	}
	
	@Override
	public PageResult list(SysAppVO app, PageVal pageVal){
		QueryWrapper<SysApp> queryWrapper=new QueryWrapper<SysApp>();
		queryWrapper.eq(!StringUtils.isNullOrEmpty(app.getName()),"name",app.getName());
		IPage<SysApp> appDoList=appMapper.selectPage(new PageNavigation<>(pageVal),queryWrapper);
		List<SysAppVO> appVOS=BeanUtil.doToVoList(appDoList.getRecords(),SysAppVO.class);
		return new PageResult(appDoList,appVOS);
	}

	@Override
	public List<SysAppVO> list() {
		List<SysApp> sysApps=appMapper.selectList(null);
		return BeanUtil.doToVoList(sysApps,SysAppVO.class);
	}

	@Override
	public int save(SysAppVO app){
		log.info("SysAppVO新增：{}",app.toString());
		return appMapper.insert(BeanUtil.voToDo(app,SysApp.class));
	}
	
	@Override
	public int update(SysAppVO app){
		log.info("SysAppVO更新：{}",app.toString());
		return appMapper.updateById(BeanUtil.voToDo(app,SysApp.class));
	}
	
	@Override
	public int remove(Long id){
		log.info("SysAppVO删除：{}",id);
		return appMapper.deleteById(id);
	}
	
}
