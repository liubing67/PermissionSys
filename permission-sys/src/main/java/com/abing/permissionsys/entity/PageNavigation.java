package com.abing.permissionsys.entity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class PageNavigation<T> extends Page<T> {

    public PageNavigation(long current, long size) {
        super(current, size);
    }
    public PageNavigation(PageVal pageVal) {
        super(pageVal.getPage()+1, pageVal.getSize());
    }
}
