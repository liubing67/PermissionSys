package com.abing.permissionsys.timing.task;

import com.abing.permissionsys.timing.BaseJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * 测试用
 * @author Zheng Jie
 * @date 2019-01-08
 */
@Slf4j
@Component
public class TestTask implements BaseJob{

    @Override
    public void run() {
        log.info("执行成功--------------");
    }
}
