package com.abing.permissionsys.enums;

public class UserEnums {


    public enum UserStatusEnum{

        /**
         * 状态 0:禁用，1:正常
         */
        UserStatusEnum_0("0","禁用"),
        UserStatusEnum_1("1","正常");
        private String index;
        private String name;


        UserStatusEnum(String index,String name){
            this.index=index;
            this.name=name;
        }
        public static String getName(String index) {
            for (UserStatusEnum code : UserStatusEnum.values()) {
                if (code.getIndex().equals(index)) {
                    return code.name;
                }
            }
            return null;
        }

        public static String getIndex(String name) {
            for (UserStatusEnum code : UserStatusEnum.values()) {
                if (code.getName().equals(name)) {
                    return code.index;
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }
    }
}
