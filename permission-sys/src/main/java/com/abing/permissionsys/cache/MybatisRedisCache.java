package com.abing.permissionsys.cache;


import com.abing.core.utils.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * mybatis二级缓存整合Redis
 */
@Slf4j
public class MybatisRedisCache implements Cache {


//    @Autowired
//    public RedisUtil redisUtil;


    // 读写锁
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    private String id;



    public MybatisRedisCache(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        log.info("Redis Cache id " + id);
        this.id = id;
    }
    public String getId() {
        return this.id;
    }
    private RedisTemplate<Object, Object> getRedisTemplate(){
        return SpringContextHolder.getBean("redisTemplate");
    }

    @Override
    public void putObject(Object key, Object value) {
        // TODO Auto-generated method stub
        if(value!=null) {
            // 向Redis中添加数据，有效时间是2天
            log.info("putObject---key----->"+key.toString());
            log.info("putObject---value----->"+value);
            getRedisTemplate().opsForValue().set(key.toString(), value, (long) 60, TimeUnit.MINUTES);
        }
    }
    @Override
    public Object getObject(Object key) {
        // TODO Auto-generated method stub
        try {
            if(key!=null) {
                Object obj = getRedisTemplate().opsForValue().get(key.toString());
                log.info("getObject---key----->"+key.toString());
                log.info("getObject---value----->"+obj);

                return	obj;
            }
        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            log.error("redis getObject error:"+e);
        }
        return null;
    }
    @Override
    public Object removeObject(Object key) {
        // TODO Auto-generated method stub
        try {
            if (key != null) {
                getRedisTemplate().delete(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void clear() {
        // TODO Auto-generated method stub
        log.info("清空缓存");
        try {
            Set<Object> keys = getRedisTemplate().keys("*:" + this.id + "*");
            if (!CollectionUtils.isEmpty(keys)) {
                getRedisTemplate().delete(keys);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    @Override
    public int getSize() {
        // TODO Auto-generated method stub
        Long size = getRedisTemplate().execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.dbSize();
            }
        });
        return size.intValue();

    }
    @Override
    public ReadWriteLock getReadWriteLock() {
        // TODO Auto-generated method stub
        return this.readWriteLock;
    }
}
