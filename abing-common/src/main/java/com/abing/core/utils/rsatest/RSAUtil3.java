package com.abing.core.utils.rsatest;

import org.apache.commons.codec.binary.Base64;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * RSA加解密工具类
 * 私钥加密公钥解密，或公钥加密私钥解密
 * signature类用于提供数字签名，用于保证数据的完整性
 */
public class RSAUtil3 {
    /**
     * 签名算法
     */
    private static final String SIGN_ALGORITHMS = "SHA256WithRSA";
    /**
     * 加载公钥
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    private static PublicKey loadPublicKey(String filePath) throws Exception {
        FileReader fr = new FileReader(filePath);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder keyString = new StringBuilder();
        String str;
        while ((str = br.readLine()) != null) {
            keyString.append(str);
        }
        br.close();
        fr.close();
        return getPublicKey(keyString.toString());
    }

    /**
     * 加载私钥
     *
     * @param filePath
     * @return
     * @throws Exception
     */
    private static PrivateKey loadPrivateKey(String filePath) throws Exception {
        FileReader fr = new FileReader(filePath);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder keyString = new StringBuilder();
        String str;
        while ((str = br.readLine()) != null) {
            keyString.append(str);
        }
        br.close();
        fr.close();
        return getPrivateKey(keyString.toString());
    }

    /**
     * 获取公钥
     *
     * @param publicKeyString
     * @return
     * @throws Exception
     */
    private static PublicKey getPublicKey(String publicKeyString) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] encodedKey = Base64.decodeBase64(publicKeyString);
        return keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
    }

    /**
     * 获取私钥
     *
     * @param privateKeyString
     * @return
     * @throws Exception
     */
    private static PrivateKey getPrivateKey(String privateKeyString) throws Exception {
        PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(
                Base64.decodeBase64(privateKeyString));
        KeyFactory keyf = KeyFactory.getInstance("RSA");
        return keyf.generatePrivate(encodedKeySpec);
    }

    /**
     * 加密
     */
    public static String encrypt(String content, PrivateKey priKey) throws Exception {
        Signature signature = Signature
                .getInstance(SIGN_ALGORITHMS);
        signature.initSign(priKey);
        signature.update(content.getBytes(StandardCharsets.UTF_8));
        byte[] signed = signature.sign();
        return Base64.encodeBase64String(signed);
    }

    /**
     * 解密
     */
    public static byte[] decrypt(Key key, byte[] data) throws Exception {

        return null;
    }
    /**
     * 验签
     * @author : liubing
     * @date : 2021/4/19 11:34
     * @param content
     * @param sign
     * @param publicKey
     * @return boolean
     */
    public static boolean doCheck(String content, String sign, PublicKey publicKey) throws Exception{
        Signature signature = Signature
                .getInstance(SIGN_ALGORITHMS);
        signature.initVerify(publicKey);
        signature.update(content.getBytes());
        return signature.verify(Base64.decodeBase64(sign));
    }

    /**
     *根据私钥证书路径获取签名
     * @author : liubing
     * @date : 2021/4/19 11:26
     * @param content 内容
     * @param privateKeyPath 私钥路径
     * @return java.lang.String
     */
    public static String sign(String content, String privateKeyPath) throws Exception {
        return encrypt(content,loadPrivateKey(privateKeyPath));
    }
    /**
     *根据公钥证书路径检验签名
     * @author : liubing
     * @date : 2021/4/19 11:26
     * @param content 内容
     * @param publicKeyPath 私钥路径
     * @return java.lang.String
     */
    public static boolean checkSign(String content, String sign, String publicKeyPath) throws Exception {
        return doCheck(content,sign,loadPublicKey(publicKeyPath));
    }

}