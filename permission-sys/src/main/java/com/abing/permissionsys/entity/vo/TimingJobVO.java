package com.abing.permissionsys.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-06 16:05:18
 */
@Data
public class TimingJobVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 任务调度参数key
	 */
	public static final String JOB_PARAM_KEY = "JOB_PARAM_KEY";
	//ID
	private Long id;
	//任务类名
	private String className;
	//cron 表达式
	private String cronExpression;
	//状态：1暂停、0启用
	private String state;
	//描述
	private String params;
	//备注
	private String remark;
	//创建日期
	private Date createTime;
	//上次执行时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date prevExecuteTime;
	//下次执行时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date nextExecuteTime;

}
