package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * 部门管理
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 15:35:39
 */
public interface DeptMapper extends BaseMapper<SysDept> {

	List<SysDept> list(SysDept sysDept);

	int count(Map<String, Object> map);

	int getDeptUserNumber(Long deptId);
}
