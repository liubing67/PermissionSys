package com.abing.base;

import com.abing.constants.Constants;
import com.abing.constants.HttpResCode;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 
 * 
 * @description: 微服务接口实现该接口可以使用传递参数可以直接封装统一返回结果集
 */
@Data
@Component
public class BaseApiService<T> {

	//直接返回枚举类型
	public BaseResponse<T> setResultError(HttpResCode httpResCode) {
		return setResult(httpResCode.getCode(), httpResCode.getValue(), null);
	}
	public BaseResponse<T> setResultError(Integer code, String msg) {
		return setResult(code, msg, null);
	}

	// 返回错误，可以传msg
	public BaseResponse<T> setResultError(String msg) {
		return setResult(HttpResCode.HttpResCode_500.getCode(), msg, null);
	}
	// 返回错误，不传msg
	public BaseResponse<T> setResultError() {
		return setResult(HttpResCode.HttpResCode_500.getCode(), HttpResCode.HttpResCode_500.getValue(), null);
	}
	// 返回成功，可以传data值
	public BaseResponse<T> setResultSuccess(T data) {
		return setResult(HttpResCode.HttpResCode_200.getCode(), HttpResCode.HttpResCode_200.getValue(), data);
	}

	// 返回成功，沒有data值
	public BaseResponse<T> setResultSuccess() {
		return setResult(HttpResCode.HttpResCode_200.getCode(), HttpResCode.HttpResCode_200.getValue(), null);
	}

	// 返回成功，沒有data值
	public BaseResponse<T> setResultSuccess(String msg) {
		return setResult(HttpResCode.HttpResCode_200.getCode(), msg, null);
	}

	// 通用封装
	public BaseResponse<T> setResult(Integer code, String msg, T data) {
		return new BaseResponse<T>(code, msg, data);
	}

	// 调用数据库层判断
	public Boolean toDaoResult(int result) {
		return result > 0 ? true : false;
	}

	// 接口直接返回true 或者false
	public Boolean isSuccess(BaseResponse<?> baseResp) {
		if (baseResp == null) {
			return false;
		}
		if (baseResp.getCode().equals(HttpResCode.HttpResCode_500.getCode())) {
			return false;
		}
		return true;
	}
}
