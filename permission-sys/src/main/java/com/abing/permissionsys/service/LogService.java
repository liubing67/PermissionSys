package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysLogVO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.scheduling.annotation.Async;

/**
 * 系统日志
 *
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-11-12 19:42:50
 */
public interface LogService {

	PageResult list(SysLogVO app, PageVal pageVal);

	int save(SysLogVO log);

	@Async("getAsyncExecutor")
	void save(String username, String browser, String ip, ProceedingJoinPoint joinPoint, SysLogVO sysLogVO);

	int update(SysLogVO log);

	int remove(Long id);

}
