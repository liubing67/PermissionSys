package com.abing.permissionsys.mapper;

import com.abing.permissionsys.entity.domain.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 用户与角色对应关系
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 11:08:59
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<SysUserRole> {


	int count(Map<String, Object> map);

	int save(SysUserRole userRole);

	int update(SysUserRole userRole);

	int remove(Long id);


	List<Long> selectRoleByUserId(Long userId);

	int removeByUserId(Long userId);

	int removeByRoleId(Long roleId);

}
