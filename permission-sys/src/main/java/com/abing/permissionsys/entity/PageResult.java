/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.abing.permissionsys.entity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 *
 * @author Mark sunlightcs@gmail.com
 */
@Data
public class PageResult implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 总记录数
	 */
	private int totalCount;
	/**
	 * 每页记录数
	 */
	private int size;
	/**
	 * 总页数
	 */
	private int totalPage;
	/**
	 * 当前页数
	 */
	private int currentPage;
	/**
	 * 列表数据
	 */
	private List<?> dataList;

	/**
	 * 分页
	 * @param list        列表数据
	 * @param totalCount  总记录数
	 * @param pageSize    每页记录数
	 * @param currPage    当前页数
	 */
	public PageResult(List<?> list, int totalCount, int pageSize, int currPage) {
		this.dataList = list;
		this.totalCount = totalCount;
		this.size = pageSize;
		this.currentPage = currPage;
		this.totalPage = (int)Math.ceil((double)totalCount/pageSize);
	}

	/**
	 * 分页
	 */
	public PageResult(IPage<?> page) {
		this.dataList = page.getRecords();
		this.totalCount = (int)page.getTotal();
		this.size = (int)page.getSize();
		this.currentPage = (int)page.getCurrent();
		this.totalPage = (int)page.getPages();
	}
	/**
	 * 分页
	 */
	public PageResult(IPage<?> page, List<?> list) {
		this.dataList = list;
		this.totalCount = (int)page.getTotal();
		this.size = (int)page.getSize();
		this.currentPage = (int)page.getCurrent();
		this.totalPage = (int)page.getPages();
	}


}
