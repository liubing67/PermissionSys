package com.abing.permissionsys.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 新版财务系统-下拉框专用VO
 * yuliren 2016.6
 */
@Data
public class ComboBoxVO implements Serializable{
	
	/**
	 * 
	 */
	private String value;
	private String name;

	
	public ComboBoxVO() {
		
	}
	
	public ComboBoxVO(String value, String name) {
		this.value = value;
		this.name=name;
		
	}
		
}
