package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysAppVO;

import java.util.List;

/**
 * 应用系统表
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-06-21 10:54:03
 */
public interface AppService {

	SysAppVO get(Long id);

	PageResult list(SysAppVO app, PageVal pageVal);

	List<SysAppVO> list();


	int save(SysAppVO app);

	int update(SysAppVO app);

	int remove(Long id);

}
