package com.abing.permissionsys.limit.aspect;

/**
 * 限流枚举
 * @author /
 */
public enum LimitType {
    // 默认
    CUSTOMER,
    //  by ip addr
    IP;
}
