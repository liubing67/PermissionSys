/**
 *
 */
package com.abing.core.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Name SessionUtil
 * @Descr session操作
 * @author yanbin
 * @date 2018年04月27日
 */
public class SessionUtil {

	private final static int outTime=60*60;

	public static void setSessionObj(String key,Object obj) {
		HttpSession session=getsession();
		session.setAttribute(key, obj);
		session.setMaxInactiveInterval(outTime);
	}

	public static Object getSessionObj(String key) {

		HttpSession session=getsession();
		return session.getAttribute(key);


	}

	public static void clearAttribute(String key) {
		HttpSession session=getsession();
		session.removeAttribute(key);
	}



	private static Object create(String className) {
		Object obj=null;
		try {
			obj= Class.forName(className).newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}






	public static HttpSession getsession() {
		return getRequest().getSession();
	}

	public static HttpServletRequest getRequest() {
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

		HttpServletRequest request = requestAttributes.getRequest();

		return request;
	}
}
