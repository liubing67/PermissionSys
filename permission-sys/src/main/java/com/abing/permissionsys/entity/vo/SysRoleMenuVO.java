package com.abing.permissionsys.entity.vo;

import lombok.Data;

import java.io.Serializable;


/**
 * 角色与菜单对应关系
 * 
 * @author liubing
 * @email 724266839@qq.com
 * @date 2019-09-11 19:57:27
 */
@Data
public class SysRoleMenuVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//角色ID
	private Long roleId;
	//菜单ID
	private Long menuId;

}
