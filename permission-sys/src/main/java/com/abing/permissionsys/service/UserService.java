package com.abing.permissionsys.service;

import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysUserVO;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
	SysUserVO selectOne(SysUserVO sysUserVO);

	PageResult list(SysUserVO app, PageVal pageVal);

	int save(SysUserVO sysUserVO);

	int update(SysUserVO sysUserVO);

	int remove(Long userId);

}
