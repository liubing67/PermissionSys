package com.abing.core.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 对比两个数据是否相等
 * 对比两个对象属性是否相等
 *
 */
public class CompareTwoUtils {

	//对比两个数据是否相等
	public static boolean compareTwo(Object object1,Object object2){  
		  
	    if(object1==null&&object2==null){  
	        return true;  
	    }
	    if(isEmpty(object1)&&isEmpty(object2)){
	    	return true; 
	    }
	    if(object1==null&&object2!=null){  
	        return false;  
	    }  
	    if(object1.equals(object2)){  
	        return true;  
	    }  
	    return false;  
	}  
	
	//找出两个对象属性不同的
	public static List<String> compareTwoClass(Object class1,Object class2) throws ClassNotFoundException, IllegalAccessException {  
	    List<String> list=new ArrayList<String>();  
	//获取对象的class  
	    Class<?> clazz1 = class1.getClass();  
	    Class<?> clazz2 = class2.getClass();  
	//获取对象的属性列表  
	    Field[] field1 = clazz1.getDeclaredFields();  
	    Field[] field2 = clazz2.getDeclaredFields();  
	//遍历属性列表field1  
	    for(int i=0;i<field1.length;i++){  
	//遍历属性列表field2  
	        for(int j=0;j<field2.length;j++){  
	//如果field1[i]属性名与field2[j]属性名内容相同  
	         if(field1[i].getName().equals(field2[j].getName())){  
	                field1[i].setAccessible(true);  
	                field2[j].setAccessible(true);  
	//如果field1[i]属性值与field2[j]属性值内容不相同  
	                if (!compareTwo(field1[i].get(class1), field2[j].get(class2))){  
	                   list.add(field1[i].getName()); 
	                }  
	                break;  
	          }  
	         }  
	     }  
	   
	     return list;  
	 }  
	
	
	  public static boolean isEmpty(Object obj)  
	    {  
	        if (obj == null)  
	        {  
	            return true;  
	        }  
	        if ((obj instanceof List))  
	        {  
	            return ((List) obj).size() == 0;  
	        }  
	        if ((obj instanceof String))  
	        {  
	            return ((String) obj).trim().equals("");  
	        }  
	        return false;  
	    }  
}
