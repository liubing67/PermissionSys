package com.abing.permissionsys.service.impl;


import cn.hutool.json.JSONObject;
import com.abing.core.utils.BeanUtil;
import com.abing.permissionsys.entity.PageNavigation;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.domain.SysApp;
import com.abing.permissionsys.entity.domain.SysLog;
import com.abing.permissionsys.entity.vo.SysAppVO;
import com.abing.permissionsys.entity.vo.SysLogVO;
import com.abing.permissionsys.log.aop.Log;
import com.abing.permissionsys.mapper.LogMapper;
import com.abing.permissionsys.service.LogService;
import com.abing.permissionsys.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.List;


@Service
public class LogServiceImpl implements LogService {
	@Autowired
	private LogMapper logMapper;


	@Override
	public PageResult list(SysLogVO app, PageVal pageVal) {
		QueryWrapper<SysLog> queryWrapper=new QueryWrapper<SysLog>();
		queryWrapper.eq(!com.mysql.jdbc.StringUtils.isNullOrEmpty(app.getUsername()),"username",app.getUsername());
		queryWrapper.eq(!com.mysql.jdbc.StringUtils.isNullOrEmpty(app.getLogType()),"log_type",app.getLogType());
		queryWrapper.orderByDesc("gmt_create");
		IPage<SysLog> appDoList=logMapper.selectPage(new PageNavigation<>(pageVal),queryWrapper);
		List<SysLogVO> logVOS= BeanUtil.doToVoList(appDoList.getRecords(),SysLogVO.class);
		return new PageResult(appDoList,logVOS);
	}

	@Override
	public int save(SysLogVO log) {
		return logMapper.insert(BeanUtil.change(log,SysLog.class));
	}

	@Override
//	@Transactional(rollbackFor = Exception.class)
	public void save(String username, String browser, String ip, ProceedingJoinPoint joinPoint, SysLogVO sysLogVO) {


		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		Log aopLog = method.getAnnotation(Log.class);

		// 方法路径
		String methodName = joinPoint.getTarget().getClass().getName()+"."+signature.getName()+"()";

		StringBuilder params = new StringBuilder("{");
		//参数值
		Object[] argValues = joinPoint.getArgs();
		//参数名称
		String[] argNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
		if(argValues != null){
			for (int i = 0; i < argValues.length; i++) {
				params.append(" ").append(argNames[i]).append(": ").append(argValues[i]);
			}
		}

		SysLog sysLog=new SysLog();


		// 描述
		if (aopLog!=null){
			sysLog.setOperation(aopLog.value());
		}

		sysLog.setIp(ip);

		String LOGINPATH = "login";
		if(LOGINPATH.equals(signature.getName())){
			try {
				assert argValues != null;
				username = new JSONObject(argValues[0]).get("username").toString();
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		sysLog.setAddress(StringUtils.getCityInfo(sysLog.getIp()));
		sysLog.setMethod(methodName);
		sysLog.setUsername(username);
		sysLog.setParams(params.toString() + " }");
		sysLog.setBrowser(browser);
		sysLog.setLogType(sysLogVO.getLogType());
		sysLog.setTime(sysLogVO.getTime());
		sysLog.setExceptionDetail(sysLogVO.getExceptionDetail());
		logMapper.insert(sysLog);
	}

	@Override
	public int update(SysLogVO log) {
		return 0;
	}

	@Override
	public int remove(Long id) {
		return 0;
	}
}
