package com.abing.permissionsys.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class PageVal implements Serializable {

    /**
     * 分页大小
     */
    private int size=10;
    /**
     * 当前页
     */
    private int page=1;
}
