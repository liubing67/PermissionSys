package com.abing.permissionsys.controller;


import com.abing.base.BaseResponse;
import com.abing.core.utils.EncryptUtils;
import com.abing.permissionsys.entity.PageResult;
import com.abing.permissionsys.entity.PageVal;
import com.abing.permissionsys.entity.vo.SysUserVO;
import com.abing.permissionsys.service.RoleService;
import com.abing.permissionsys.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
@Slf4j
public class UserController extends BaseController {
	@Autowired
    UserService userService;
	@Autowired
	RoleService roleService;

    @ResponseBody
    @RequestMapping(value = "/listPage")
    public BaseResponse listPage(SysUserVO userVO, PageVal pageVal){
        //查询列表数据
		log.info("SysUserVO查询：{}",userVO);
		PageResult appDOIPage=userService.list(userVO,pageVal);
        log.info("SysUserVO查询返回：{}",appDOIPage);
        return setResultSuccess(appDOIPage);
    }

	@PostMapping("/save")
	@ResponseBody
    BaseResponse save(@RequestBody SysUserVO user) {
		user.setPassword(EncryptUtils.encryptMD5("123456"));
		if (userService.save(user) > 0) {
			return setResultSuccess();
		}
		return setResultError();
	}

	@PostMapping("/update")
	@ResponseBody
    BaseResponse update(@RequestBody SysUserVO user) {
		if (userService.update(user) > 0) {
			return setResultSuccess();
		}
		return setResultError();
	}

	@PostMapping("/remove/{id}")
	@ResponseBody
    BaseResponse remove(@PathVariable Long id) {
		if (userService.remove(id) > 0) {
			return setResultSuccess();
		}
		return setResultError();
	}
}
