package com.abing.permissionsys.controller;

import com.abing.base.BaseApiService;
import com.abing.permissionsys.security.security.JwtUser;
import com.abing.permissionsys.utils.SecurityUtils;

public class BaseController extends BaseApiService {
	public JwtUser getUser() {
		return (JwtUser) SecurityUtils.getUserDetails();
	}

	public Long getUserId() {
		return getUser().getId();
	}

	public String getUsername() {
		return getUser().getUsername();
	}
}